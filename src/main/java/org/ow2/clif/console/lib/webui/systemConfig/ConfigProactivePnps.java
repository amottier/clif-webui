package org.ow2.clif.console.lib.webui.systemConfig;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
/**
 * Reads the application.properties file specified when lanching CLIF Webui.
 * Retrieves the properties containing "proactive.pnps"
 * @author Antoine Thevenet
 *
 */
@Configuration
@ConfigurationProperties(prefix = "proactive.pnps")
public class ConfigProactivePnps {
	
	private String port = null;
	private String idle_timeout = null;
	private String default_heartbeat = null;
	private String authenticate = null;
	private String keystore = null;
	
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getIdle_timeout() {
		return idle_timeout;
	}
	public void setIdle_timeout(String idle_timeout) {
		this.idle_timeout = idle_timeout;
	}
	public String getDefault_heartbeat() {
		return default_heartbeat;
	}
	public void setDefault_heartbeat(String default_heartbeat) {
		this.default_heartbeat = default_heartbeat;
	}
	public String getAuthenticate() {
		return authenticate;
	}
	public void setAuthenticate(String authenticate) {
		this.authenticate = authenticate;
	}
	public String getKeystore() {
		return keystore;
	}
	public void setKeystore(String keystore) {
		this.keystore = keystore;
	}

}
