package org.ow2.clif.console.lib.webui.systemConfig;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
/**
 * Reads the application.properties file specified when lanching CLIF Webui.
 * Retrieves the properties containing "proactive.pamrssh"
 * @author Antoine Thevenet
 *
 */
@Configuration
@ConfigurationProperties(prefix = "proactive.pamrssh")
public class ConfigProactivePamrssh {
	
	private String port = null;
	private String username = null;
	private String key_directory = null;
	private String address = null;
	
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getKey_directory() {
		return key_directory;
	}
	public void setKey_directory(String key_directory) {
		this.key_directory = key_directory;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
}
