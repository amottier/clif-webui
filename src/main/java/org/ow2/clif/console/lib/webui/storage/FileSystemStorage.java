/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2020 Tim Martin, Antoine Thevenet
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.console.lib.webui.storage;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import javax.lang.model.element.Name;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.util.FileSystemUtils;
import org.springframework.util.ResourceUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.ow2.clif.console.lib.webui.storage.Exceptions.*;
import org.ow2.clif.console.lib.webui.util;

/**
 *
 * @author Tim Martin
 * @author Antoine Thevenet
 */

public class FileSystemStorage {

	private final Path rootLocation;

	Logger logger = (Logger) LoggerFactory.getLogger(FileSystemStorage.class);

	public FileSystemStorage(String rootLocation) {
		this.rootLocation = Paths.get(rootLocation);
		this.init();
	}

	public FileSystemStorage() {
		this.rootLocation = Paths.get(System.getProperty("user.dir") + "/test/");
	}

	public void store(String file, String content) {
		try {
			if (file.isEmpty()) {
				throw new StorageException("Failed to store empty file " + file);
			}
			if (file.contains("..")) { // This is a security check
				throw new StorageException("Cannot store file with relative path outside current directory " + file);
			} else if (this.rootLocation.resolve(file).toFile().exists()) {
				throw new StorageException(this.rootLocation.resolve(file) + " already exists");
			}
			File f = new File(this.rootLocation.toString() + '/' + file);
			f.createNewFile();
			FileWriter myWriter = new FileWriter(this.rootLocation.toString() + '/' + file);
			myWriter.write(content);
			myWriter.close();

		} catch (IOException e) {
			throw new StorageException("Failed to store file " + file, e);
		}
	}

	public void createFile(String fileName) {
		try {
			if (fileName.contains("..") || fileName.startsWith("/")) {
				throw new StorageException(
						"Cannot store file with relative path outside current directory " + fileName);
			} else if (!(fileName.contains("/"))) {
				throw new StorageException("Cannot store file in root directory: " + fileName);
			}
			if (this.rootLocation.resolve(fileName).toFile().exists()) {
				throw new StorageException(this.rootLocation.resolve(fileName) + " already exists");
			} else {
				File file = new File(this.rootLocation.toString() + '/' + fileName);
				file.createNewFile();
			}
		} catch (IOException e) {
			throw new StorageException("Failed to store file " + this.rootLocation + '/' + fileName, e);
		}
	}

	/**
	 * Creates a new file with a predefined content. The predefined content depends
	 * on the fileName extension. The predefined content is defined in the
	 * resources/templates/file
	 * 
	 * @param fileName the String that defines the filename
	 */
	public void createFileWithContent(String fileName) {
		String extension = fileName.substring(fileName.length() - 4);
		try {
			if (fileName.contains("..") || fileName.startsWith("/")) {
				throw new StorageException(
						"Cannot store file with relative path outside current directory " + fileName);
			} else if (!(fileName.contains("/"))) {
				throw new StorageException("Cannot store file in root directory: " + fileName);
			}
			if (this.rootLocation.resolve(fileName).toFile().exists()) {
				throw new StorageException(this.rootLocation.resolve(fileName) + " already exists");
			} else if (extension.equals(".ctp")) {
				// create new file
				File newFile = new File(this.rootLocation.toString() + '/' + fileName);
				newFile.createNewFile();
				String firstPathname = this.rootLocation.toString() + '/' + fileName;
				System.out.println(new ClassPathResource("templates/file/skeleton/ctpSkeleton.txt"));
				InputStream resource = new ClassPathResource("templates/file/skeleton/ctpSkeleton.txt")
						.getInputStream();
				BufferedReader reader = new BufferedReader(new InputStreamReader(resource));
				String content = reader.lines().collect(Collectors.joining("\n"));

				// write data from fileSkeleton class
				FileWriter myWriter = new FileWriter(firstPathname);
				myWriter.write(content);
				myWriter.close();
			} else if (extension.equals(".xis")) {
				File file = new File(this.rootLocation.toString() + '/' + fileName);
				file.createNewFile();

				InputStream resource = new ClassPathResource("templates/file/skeleton/xisSkeleton.xml")
						.getInputStream();
				BufferedReader reader = new BufferedReader(new InputStreamReader(resource));
				String content = reader.lines().collect(Collectors.joining("\n"));

				FileWriter myWriter = new FileWriter(this.rootLocation.toString() + '/' + fileName);
				myWriter.write(content);
				myWriter.close();
			} else {
				throw new StorageException("Invalid extension " + fileName);
			}
		} catch (IOException e) {
			throw new StorageException("Failed to store file " + this.rootLocation + '/' + fileName, e);

		}
	}

	public void duplicateFile(String duplicatedFileName, String originalFileName) throws IOException {
		File newFile = new File(this.rootLocation.toString() + '/' + duplicatedFileName);
		newFile.createNewFile();
		String firstPathname = this.rootLocation.toString() + '/' + duplicatedFileName;
		String originalFileContent = loadContent(originalFileName);
		FileWriter myWriter = new FileWriter(firstPathname);
		myWriter.write(originalFileContent);
		myWriter.close();
	}

	public void createDirectory(String path) {
		if (path.endsWith("/")) { // fail to resolve path if we don't remove potential slash at the end
			path = path.substring(0, path.length() - 1);
		}
		logger.info("Creating directory " + path);
		if (path.contains("..") || path.startsWith("/")) {
			throw new StorageException("Cannot screate directory with relative path outside current directory " + path);
		}
		if (path.lastIndexOf("/") > 0
				&& !(this.rootLocation.resolve(path.substring(0, path.lastIndexOf("/"))).toFile().exists())) {
			throw new StorageException(path + ": path to directory doesn't exists");
		}
		if (this.rootLocation.resolve(path).toFile().exists()) {
			throw new StorageException(path + " already exists");
		}
		if (path.isEmpty()) {
			throw new StorageException(path + " must not be null");
		}
		try {
			Files.createDirectories(Paths.get(this.rootLocation.toString() + '/' + path));
			if (!path.contains("/")) { // dir created was a project
				util.createPaclifOpts(this.rootLocation.toString() + '/' + path + "/paclif.opts");
			}

		} catch (IOException e) {
			throw new StorageException("Failed to store file " + this.rootLocation + '/' + path, e);
		}
	}

	public void rename(String newName, String path) {
		if (newName.contains("..") || path.contains("..") || newName.contains("..") || newName.contains("%20")) {
			// This is a security check
			throw new StorageException("Cannot store file with relative path outside current directory " + newName);
		}
		if (newName.contains("/")) {
			throw new StorageException("Cannot move file by renaming it");
		}

		if (elementExists(path)) {
			try {
				Path newPath;
				if (path.contains("/")) { // is no a project
					newPath = Paths.get(this.rootLocation.toString() + '/'
							+ path.substring(0, path.lastIndexOf('/') + 1) + newName);
				} else {
					newPath = Paths.get(this.rootLocation.toString() + '/' + newName);
				}
				File newFile = newPath.toFile();
				if (newFile.exists()) {
					throw new StorageException(path + " already exists");
				}
				Files.move(Paths.get(this.rootLocation.toString() + '/' + path), newPath);
			} catch (IOException e) {
				throw new StorageException("Failed to rename " + path);
			}
		} else {
			throw new StorageException(path + " does not exists");
		}

	}

	public void move(String file, String path) {

		if (path.contains("..") || path.contains("%20")) { // This is a security check
			throw new StorageException("Cannot store file with relative path outside current directory " + path);
		}

		path = this.rootLocation + "/" + path;

		if (!new File(this.rootLocation + "/" + file).exists())
			throw new StorageException(file + " does not exists");
		if (!new File(path).exists())
			throw new StorageException(path + " does not exists");
		if (!new File(path).isDirectory())
			throw new StorageException(path + " is not a directory");

		String pathString;
		if (path.endsWith("/"))
			pathString = path + file.substring(file.lastIndexOf("/"));
		else
			pathString = path + "/" + file.substring(file.lastIndexOf("/"));

		try {
			Path newPath = Paths.get(pathString);
			File newFile = newPath.toFile();
			if (newFile.exists()) {
				throw new StorageException(pathString + " already exists");
			}
			Files.move(Paths.get(this.rootLocation.toString() + '/' + file), newPath);
		} catch (IOException e) {
			throw new StorageException("Failed to move " + file + " to " + pathString);
		}

	}

	public void delete(String path) {
		while (path.startsWith(" "))
			path.replaceFirst(" ", "");
		if (path.contains("..") || path.startsWith("/") || path == "." || path.isEmpty())
			throw new StorageException("Cannot delete file outside of relative path");
		if (this.rootLocation.resolve(path).toFile().exists()) {
			File file = new File(this.rootLocation.toString() + '/' + path);
			if (file.isDirectory()) {
				try {
					FileUtils.deleteDirectory(file);
				} catch (IOException e) {
					throw new StorageException("Failed to delete delete " + path);
				}
			} else {
				if (!file.delete()) {
					throw new StorageException("Failed to delete " + path);
				}
			}
		} else {
			throw new StorageException("Cannot delete " + path + ": element does not exist");
		}

	}

	public Stream<Path> loadAll() {
		try {
			return Files.walk(this.rootLocation).filter(path -> !path.equals(this.rootLocation))
					.map(this.rootLocation::relativize);
		} catch (IOException e) {
			throw new StorageException("Failed to read stored files", e);
		}

	}

	public List<String> loadProjects() throws IOException {
		List<Path> listPath = (Files.walk(this.rootLocation, 1).filter(path -> !path.equals(this.rootLocation))
				.map(this.rootLocation::relativize)).collect(Collectors.toList());
		List<String> listString = new ArrayList<>();
		for (int i = 0; i < listPath.size(); i++) {
			listString.add(listPath.get(i).toString());
		}
		return listString;
	}

	public Path load(String filename) {
		return rootLocation.resolve(filename);
	}

	public Resource loadAsResource(String filename) {
		try {
			Path file = load(filename);
			Resource resource = new UrlResource(file.toUri());
			if (resource.exists() || resource.isReadable()) {
				return resource;
			} else {
				throw new StorageFileNotFoundException("Could not read file: " + filename);
			}
		} catch (MalformedURLException e) {
			throw new StorageFileNotFoundException("Could not read file: " + filename, e);
		}
	}

	public String saveFile(String filename, String fileContent) throws FileNotFoundException {
		PrintWriter prw = new PrintWriter(this.rootLocation.toString() + '/' + filename);
		prw.println(fileContent);
		prw.close();
		return "File saved";
	}

	public String loadContent(String filename) {
		try {
			String str = "";
			Scanner sc = new Scanner(new File(this.rootLocation + "/" + filename));
			while (sc.hasNextLine()) {
				str += sc.nextLine() + "%0A";
			}
			return str;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public boolean elementExists(String elementName) {
		File file = new File(this.rootLocation + "/" + elementName);
		return file.exists();
	}

	public File loadAsFile(String filename) {
		File file = new File(this.rootLocation.toString() + '/' + filename);
		return file;
	}

	public void deleteAll() {
		FileSystemUtils.deleteRecursively(rootLocation.toFile());
	}

	public void init() {
		try {
			Files.createDirectories(rootLocation);
		} catch (IOException e) {
			throw new StorageException("Could not initialize storage", e);
		}
	}

	public Stream<Path> loadDirectory() {
		try {
			return Files.walk(this.rootLocation).filter(path -> !path.equals(this.rootLocation))
					.filter(Files::isDirectory).map(this.rootLocation::relativize);
		} catch (IOException e) {
			throw new StorageException("Failed to read stored files", e);
		}
	}

	public ArrayList<String> importUnzipProject(byte[] data) throws IOException {
		ArrayList<String> projectList = new ArrayList<>();
		logger.info("unzipping project...");
		InputStream is = new ByteArrayInputStream(data);
		ZipInputStream zis = new ZipInputStream(is);
		ZipEntry zipEntry = zis.getNextEntry();
		byte[] buffer = new byte[1024];
		while (zipEntry != null) {
			String entryName = zipEntry.getName();
			logger.info("entry : " + entryName);
			if (new File(this.rootLocation + "/" + entryName).exists())
				throw new StorageException("Project already exists");
			if (zipEntry.isDirectory()) {
				if (!entryName.substring(0, entryName.length() - 1).contains(("/"))) { // dir is a project
					projectList.add(entryName);
				}

				try {
					createDirectory(entryName);
				} catch (Exception e) {
					logger.info("Exception caught : " + e.getLocalizedMessage());
					throw e;
				}

			} else {
				if (!entryName.contains("/")) {
					throw new StorageException("No files allowed in archive root.");
				}
				Path filePath = Paths.get(this.rootLocation + "/").resolve(entryName);
				File file = filePath.toFile();
				if (!file.getParentFile().exists()) {
					if (file.getParentFile().exists())
						throw new StorageException("Project " + file.getParentFile().toPath() + " already exists");
					logger.info("creating new directory " + file.getParentFile().getCanonicalPath());
					Files.createDirectories(file.getParentFile().toPath());
					String projectCreated = file.getParentFile().getCanonicalPath();
					projectCreated = projectCreated.substring(projectCreated.lastIndexOf("/")); // getting only the
																								// project name
					projectList.add(projectCreated);
				}
				logger.info("Creating  " + filePath.toString());
				file.createNewFile();
				try {
					try (FileOutputStream fos = new FileOutputStream(file);
							BufferedOutputStream bos = new BufferedOutputStream(fos, buffer.length)) {

						int len;
						while ((len = zis.read(buffer)) > 0) {
							bos.write(buffer, 0, len);
						}
					}
				} catch (Exception e) {
					logger.info("Exception caught : " + e.getLocalizedMessage());
					throw e;
				}
			}
			zipEntry = zis.getNextEntry();
		}
		for (String s : projectList) { // create paclif.opts in all new project
			if (!new File(this.rootLocation + "/" + s + "/paclif.opts").exists()) {
				logger.info("Creating paclif.opts at " + this.rootLocation + "/" + s + "/paclif.opts");
				util.createPaclifOpts(this.rootLocation + "/" + s + "/paclif.opts");
			}
		}
		logger.info("succesfuly created files");
		return projectList;

	}

	public InputStream exportZipProject(String project) throws IOException {
		File projectDir = new File(this.rootLocation + "/" + project);
		logger.info("Creating zip " + project + ".zip");

		if (!projectDir.exists())
			throw new StorageException(project + " does not exist");
		if (!projectDir.isDirectory() || project.contains("/"))
			throw new StorageException(project + " is not a project");

		FileOutputStream fos = new FileOutputStream(this.rootLocation + "/" + project + ".zip");
		ZipOutputStream zos = new ZipOutputStream(fos);
		try {
			zipFile(projectDir, projectDir.getName(), zos);
			zos.close();
			fos.close();
		} catch (Exception e) {
			throw new StorageException("Failed to zip " + project);
		}
		File zip = new File(this.rootLocation + "/" + project + ".zip");
		InputStream is = new FileInputStream(zip);
		zip.delete();
		return is;

	}

	private void zipFile(File fileToZip, String fileName, ZipOutputStream zipOut) throws IOException {
		if (fileToZip.isHidden()) {
			return;
		}
		if (fileToZip.isDirectory()) {
			if (fileName.endsWith("/")) {
				zipOut.putNextEntry(new ZipEntry(fileName));
				zipOut.closeEntry();
			} else {
				zipOut.putNextEntry(new ZipEntry(fileName + "/"));
				zipOut.closeEntry();
			}
			File[] children = fileToZip.listFiles();
			for (File childFile : children) {
				zipFile(childFile, fileName + "/" + childFile.getName(), zipOut);
			}
			return;
		}
		FileInputStream fis = new FileInputStream(fileToZip);
		ZipEntry zipEntry = new ZipEntry(fileName);
		zipOut.putNextEntry(zipEntry);
		byte[] bytes = new byte[1024];
		int length;
		while ((length = fis.read(bytes)) >= 0) {
			zipOut.write(bytes, 0, length);
		}
		fis.close();
	}

	////////////////////////////// cache Init///////////////////////////////
	/**
	 * This function is used to retrieve the content of each plugin file
	 * 
	 * @return 
	 * @throws IOException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 */
	public String initCache() throws IOException, ParserConfigurationException, SAXException {

		// Os dependant
		String osName = System.getProperty("os.name");

		ProcessBuilder processBuilder = new ProcessBuilder("retrievePlugins.sh");
		Process process = processBuilder.start();
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
		StringBuilder stringBuilder = new StringBuilder();


		int c = bufferedReader.read();

		while (c != -1) {
			stringBuilder.append(String.valueOf((char) c));
			c = bufferedReader.read();
		}

		String responseBis = stringBuilder.toString();
		return responseBis;
	}
	
	/**
	 * This function is used to retrieve the content of each plugin file
	 * 
	 * @return 
	 * @throws IOException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 */
	public String initCacheProbe() throws IOException, ParserConfigurationException, SAXException {

		// Os dependant
		String osName = System.getProperty("os.name");

		ProcessBuilder processBuilder = new ProcessBuilder("retrieveProbes.sh");
		Process process = processBuilder.start();
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
		StringBuilder stringBuilder = new StringBuilder();


		int c = bufferedReader.read();

		while (c != -1) {
			stringBuilder.append(String.valueOf((char) c));
			c = bufferedReader.read();
		}

		String responseBis = stringBuilder.toString();
		return responseBis;
	}

//////////////////////////////////Read properties/////////////////////////////////
	/**
	 * This function reads a .properties file to extract the plugin name, the plugin
	 * xml file name and the gui xml file name
	 * 
	 * @param folderURL          : The folder that contains every plugin
	 * @param pluginFolderURL    : The folder that contains the .properties file
	 * @param propertiesFileName : the .properties file name
	 * @return
	 * @throws IOException
	 */
	public ArrayList<String> readProperties(String folderURL, String pluginFolderURL, String propertiesFileName)
			throws IOException {

		InputStream propertiesFile = new ClassPathResource(
				(folderURL + pluginFolderURL).replaceAll("classes/", "") + "/" + propertiesFileName).getInputStream();
		BufferedReader reader = new BufferedReader(new InputStreamReader(propertiesFile));
		String lineRead = new String();
		ArrayList<String> properties = new ArrayList<String>(Arrays.asList("", "", ""));

		while ((lineRead = reader.readLine()) != null) {
			if (lineRead.contains("plugin.name")) {
				properties.set(0, lineRead.substring(lineRead.indexOf("=") + 1));
			}
			if (lineRead.contains("plugin.guiFile")) {
				properties.set(1, lineRead.substring(lineRead.indexOf("=") + 1));
			}
			if (lineRead.contains("plugin.xmlFile")) {
				properties.set(2, lineRead.substring(lineRead.indexOf("=") + 1));
			}
		}
		return properties;
	}

///////////////////////////////////XML to JSON////////////////////////////////////
	/**
	 * This function reads a xml file and converts it to a String with a JSON format
	 * 
	 * @param resourcePath : The .xml file name and URL
	 * @return
	 * @throws IOException
	 */
	public String xmlToJson(String resourcePath) throws IOException {
		InputStream resource = new ClassPathResource(resourcePath.replaceAll("classes/", "")).getInputStream();
		BufferedReader reader = new BufferedReader(new InputStreamReader(resource));
		String xml = reader.lines().collect(Collectors.joining("\n"));

		JSONObject xmlJSONObj = XML.toJSONObject(xml);
		String jsonPrettyPrintString = xmlJSONObj.toString(4);

		return jsonPrettyPrintString;
	}
}
