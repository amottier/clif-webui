package org.ow2.clif.console.lib.webui.systemConfig;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
/**
 * Reads the application.properties file specified when lanching CLIF Webui.
 * Retrieves the properties containing "proactive.pamr.agent"
 * @author Antoine Thevenet
 *
 */
@Configuration
@ConfigurationProperties(prefix = "proactive.pamr.agent")

public class ConfigProactivePamrAgent {
	private String id = null;
	private String magic_cookie = null;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMagic_cookie() {
		return magic_cookie;
	}

	public void setMagic_cookie(String magic_cookie) {
		this.magic_cookie = magic_cookie;
	}
}
