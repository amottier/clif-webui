package org.ow2.clif.console.lib.webui.systemConfig;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
/**
 * Reads the application.properties file specified when lanching CLIF Webui.
 * Retrieves the properties containing "proactive.pnp"
 * @author Antoine Thevenet
 *
 */
@Configuration
@ConfigurationProperties(prefix = "proactive.pnp")
public class ConfigProactivePnp {
	
	private String port = null;
	private String default_heartbeat = null;
	private String idle_timeout = null;
	
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getDefault_heartbeat() {
		return default_heartbeat;
	}
	public void setDefault_heartbeat(String default_heartbeat) {
		this.default_heartbeat = default_heartbeat;
	}
	public String getIdle_timeout() {
		return idle_timeout;
	}
	public void setIdle_timeout(String idle_timeout) {
		this.idle_timeout = idle_timeout;
	}
	
}
