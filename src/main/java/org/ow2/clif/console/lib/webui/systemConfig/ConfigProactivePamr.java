package org.ow2.clif.console.lib.webui.systemConfig;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
/**
 * Reads the application.properties file specified when lanching CLIF Webui.
 * Retrieves the properties containing "proactive.pamr"
 * @author Antoine Thevenet
 *
 */
@Configuration
@ConfigurationProperties(prefix = "proactive.pamr")
public class ConfigProactivePamr {

	private String connect_timeout = null;
	private String socketfactory = null;

	public String getSocketfactory() {
		return socketfactory;
	}

	public void setSocketfactory(String socketfactory) {
		this.socketfactory = socketfactory;
	}

	public String getConnect_timeout() {
		return connect_timeout;
	}

	public void setConnect_timeout(String connect_timeout) {
		this.connect_timeout = connect_timeout;
	}
}
