/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2021 Antoine Thevenet
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

/*------------------------------------------------------------------	
------------------------------Plugin--------------------------------
-------------------------------------------------------------------*/


/**
 * This function retrieve the params tag and its content for a given plugin.
 * It also scans ace editor content to select an available id.
 * Then, it calls insertPlugin to insert the plugin contents on Ace editor
 *@param {String} pluginName - the plugin Name
 *@param {String} pluginId - the id chose by the user
 */
function addPluginEditor(pluginName, pluginId) {
	var cache = localCache.get("pluginInfo");
	//var pluginId = choosePluginId(pluginName);
	var arrayPluginId = scanPluginId(pluginName);
	if (arrayPluginId.includes(pluginId)) {
		showPopup(getLocalizedText("error.plugin.id2").replace("%1", pluginName).replace("%2", pluginId));
	}
	else {
		var xmlBegin = "<use id=\"" + pluginId + "\" name=\"" + pluginName + "\">";
		var xmlEnd = "</use>";

		for (var i = 0; i < cache["plugins"].length; i++) {
			if (cache["plugins"][i]["name"] == pluginName) {
				var retrievedJson = cache["plugins"][i]["params"];
			}
		}
		var xmlConverted = "";
		if (retrievedJson != undefined && retrievedJson != "") {
			for (var i = 0; i < retrievedJson.length; i++) {
				var temp = "<param name=\"" + retrievedJson[i] + "\" value=\'\'></param>" + "\n";
				//xmlConverted += JSONtoXML(temp);
				xmlConverted += temp;
			}
			xmlBegin += "<params>";
			xmlEnd = "</params>" + xmlEnd;
		}
		var xml = xmlBegin + xmlConverted + xmlEnd;
		xml = formatXml(xml);
		//modify type="String" to value =""
		xml = xml.replaceAll("type=\"String\"", "value=\'\'");
		insertPlugin(xml);
	}
}

/**
* This function inserts automatically the retrieved plugin text at a valid position.
* It will be placed before the tag </plugins>
* @param {String} pluginContent : the text that will be inserted at the position defined by this function
*/
function insertPlugin(pluginContent) {
	var editor = ace.edit(getActiveEditor());
	var lineCount = 0;
	var responseInserted = false;
	editorLineNumber = editor.session.getLength();
	for (lineCount; lineCount < editorLineNumber && responseInserted == false; lineCount++) {
		var lineContent = editor.session.getLine(lineCount);
		//handles xml file written on a single line
		if (lineContent.includes('</plugins>')) {
			var newLine = formatPluginInsertion(lineContent, pluginContent);
			var Range = ace.require('ace/range').Range;
			editor.session.replace(new Range(lineCount, 0, lineCount, Number.MAX_VALUE), newLine);
			responseInserted = true;
		}
	}
}


/*------------------------------------------------------------------	
-----------------------------Primitive------------------------------
-------------------------------------------------------------------*/
/**
 *This function handles the primitive insertion. It retrieves the text to insert and determine where it will be inserted.
 *Then it formats it and inserts it in the Ace editor
 */
function insertPrimitive(primitiveType, pluginName, primitiveName, idPlugin) {
	//retrive the text to insert
	var lowerCasePrimitiveType = primitiveType.toLowerCase();
	var editor = ace.edit(getActiveEditor());
	var completeText = editor.getValue();
	var cache = localCache.get("pluginInfo");
	var retrievedJson = retrievePrimitive(lowerCasePrimitiveType, pluginName, primitiveName, cache);

	//transform the json object to a string
	var xmlConverted = "";
	for (var i = 0; i < retrievedJson.length; i++) {
		var temp = "<param name=\"" + retrievedJson[i] + "\" value=\'\'></param>" + "\n";
		//xmlConverted += JSONtoXML(temp);
		xmlConverted += temp;
	}

	//obtain the position where the text will be inserted
	var resultPositionToInsertPrimitive = positionToInsert();
	var cursorPositionToInsert = resultPositionToInsertPrimitive[0];
	var insertLineBefore = resultPositionToInsertPrimitive[1];
	var insertLineAfter = resultPositionToInsertPrimitive[2];
	var oneMoreTab = resultPositionToInsertPrimitive[3];
	editor.moveCursorTo(cursorPositionToInsert[1], cursorPositionToInsert[0]);

	//format the retrieved text
	var beginXml = "";
	var primitiveTypeSingular = lowerCasePrimitiveType.substring(0, lowerCasePrimitiveType.length - 1);
	beginXml += "<" + primitiveTypeSingular + " use=\"" + idPlugin + "\" name=\"" + primitiveName + "\">";
	var endXml = "</" + primitiveTypeSingular + ">";
	if (retrievedJson.length > 0) {
		beginXml += "<params>";
		endXml = "</params>" + endXml;
	}
	var completeXml = formatXml(beginXml + xmlConverted + endXml);

	//delete the "\n added by format xml
	completeXml = completeXml.substring(0, completeXml.length - 1);
	//	completeXml = addTabXml(completeXml, cursorPositionToInsert, completeText);
	completeXml = formatPreviousLine(completeXml, cursorPositionToInsert, insertLineAfter, oneMoreTab);
	if (insertLineBefore == true) {
		completeXml = "\n" + completeXml;
	}
	var Range = ace.require('ace/range').Range;
	editor.session.replace(new Range(cursorPositionToInsert[1], cursorPositionToInsert[0], cursorPositionToInsert[1], cursorPositionToInsert[0]), completeXml);
}


/**
* This function retrieves a given primitive. It reads the cache, retrieves the wanted JSON object and returns it.
* @param {String} primitiveType : the primitive type (control, sample, timer)
* @param {String} pluginName : the plugin name
* @param {String} primitiveName : the primitive name
* @param {Object} cache : the cache content
* @returns {String} the retrieved content of the primitive with an xml format
*/
retrievePrimitive = function retrievePrimitive(primitiveType, pluginName, primitiveName, cache) {
	var endFor = false;
	var retrievedJson = "";
	var i = 0;
	var j = 0;
	for (i; i < cache["plugins"].length && endFor == false; i++) {
		if (cache["plugins"][i]["name"] == pluginName) {
			for (j; j < cache["plugins"][i][primitiveType].length && endFor == false; j++) {
				if (cache["plugins"][i][primitiveType][j]["name"] == primitiveName) {
					if (cache["plugins"][i][primitiveType][j].hasOwnProperty("params")) {
						retrievedJson = cache["plugins"][i][primitiveType][j]["params"];
					}
					endFor = true;
				}
			}
		}
	}

	return retrievedJson;

}

/**
* This function finds the position where the text will be inserted;
* It also returns values that help to format the insertion
* @returns {array} An array of two elements:
*- the column where the primitive will be inserted 
*- the row where the primitive will be inserted
*/
function positionToInsert() {
	var editor = ace.edit(getActiveEditor());
	var cursorPosition = editor.selection.getCursor();
	var completeText = editor.getValue();
	var cursorPositionToInsert = [cursorPosition["column"], cursorPosition["row"]];

	//help to format the retrievedXml
	var insertLineBefore = false;
	var insertLineAfter = false;
	var oneMoreTab = false;

	//number of character before reaching the cursor position
	var cursorAbsolutePosition = 0;
	var chevronArrayLength = 0;

	//end the loop
	var booleanTemp = false;

	//counter
	let counterRowLine = 0;
	let counterRowChevron = 0;

	//Penser à traiter le cas de variables contenant une balise
	//Split the text twice: once to keep the cursor position, and once to browse the text  
	var arraySplitChevron = completeText.split(">");
	var arrayCompleteText = completeText.split("\n");

	for (var i = 0; i < arraySplitChevron.length; i++) {
		if (arraySplitChevron[i].includes("<")) {
			arraySplitChevron[i] += ">";
		}
	}

	for (var i = 0; i < arrayCompleteText.length; i++) {
		arrayCompleteText[i] += "\n";
	}

	//count the aboslute position of the cursor
	for (counterRowLine; counterRowLine < cursorPosition["row"]; counterRowLine++) {
		cursorAbsolutePosition += arrayCompleteText[counterRowLine].length;
	}
	cursorAbsolutePosition += cursorPosition["column"];

	//count the number of > before the cursor. Used to know at which index the cursor is in arraySplitChevron
	var temp = completeText.substring(0, cursorAbsolutePosition);
	var chevronOccurenceBeforeCursor = (temp.match(/>/g) || []).length;

	//count the length for the chevron array
	for (counterRowChevron; counterRowChevron < chevronOccurenceBeforeCursor; counterRowChevron++) {
		chevronArrayLength += arraySplitChevron[counterRowChevron].length;
	}

	while (booleanTemp == false) {
		//add the primitive after the '>' to the right of the cursor
		if (arraySplitChevron[chevronOccurenceBeforeCursor].includes("<behavior")
			|| arraySplitChevron[chevronOccurenceBeforeCursor].includes("</control")
			|| arraySplitChevron[chevronOccurenceBeforeCursor].includes("</sample")
			|| arraySplitChevron[chevronOccurenceBeforeCursor].includes("</timer")
			|| arraySplitChevron[chevronOccurenceBeforeCursor].includes("<then")
			|| arraySplitChevron[chevronOccurenceBeforeCursor].includes("<else")
			|| arraySplitChevron[chevronOccurenceBeforeCursor].includes("</while")
			|| arraySplitChevron[chevronOccurenceBeforeCursor].includes("</if")
			|| (arraySplitChevron[chevronOccurenceBeforeCursor].includes("</condition") && !arraySplitChevron[chevronOccurenceBeforeCursor + 1].includes("<then"))
			|| arraySplitChevron[chevronOccurenceBeforeCursor].includes("</preemptive")
			|| arraySplitChevron[chevronOccurenceBeforeCursor].includes("</nchoice")
			|| arraySplitChevron[chevronOccurenceBeforeCursor].includes("<choice")) {

			chevronArrayLength += arraySplitChevron[chevronOccurenceBeforeCursor].length;
			var j = 0;
			var temp2 = 0;
			while (temp2 < chevronArrayLength) {
				temp2 += arrayCompleteText[j].length;
				j++;
			}
			j--;
			temp2 -= arrayCompleteText[j].length;
			//The column is equal to chevronArrayLength - temp2 
			cursorPositionToInsert = [chevronArrayLength - temp2, j];

			//determine if one more tab will be added to the inserted text. Used for the primitive insertion
			if (arraySplitChevron[chevronOccurenceBeforeCursor].includes("<behavior")
				|| arraySplitChevron[chevronOccurenceBeforeCursor].includes("<then")
				|| arraySplitChevron[chevronOccurenceBeforeCursor].includes("<else")
				|| arraySplitChevron[chevronOccurenceBeforeCursor].includes("<choice")) {
				oneMoreTab = true;
			}
			insertLineBefore = true;

			booleanTemp = true;
		}
		//add the primitive after the '>'to the left of our cursor
		else if (arraySplitChevron[chevronOccurenceBeforeCursor].includes("</behavior>")
			|| arraySplitChevron[chevronOccurenceBeforeCursor].includes("</then")
			|| arraySplitChevron[chevronOccurenceBeforeCursor].includes("</else")
			|| arraySplitChevron[chevronOccurenceBeforeCursor].includes("</choice")
		) {
			cursorAbsolutePosition -= cursorPosition["column"];
			while (chevronArrayLength <= cursorAbsolutePosition) {
				counterRowLine -= 1;
				cursorAbsolutePosition -= arrayCompleteText[counterRowLine].length;
			}
			cursorPositionToInsert = [chevronArrayLength - cursorAbsolutePosition, counterRowLine];

			insertLineBefore = true;
			if (arraySplitChevron[chevronOccurenceBeforeCursor].includes("</behavior>")) {
				insertLineAfter = false;
				oneMoreTab = false;
			}
			else {
				insertLineAfter = true;
				oneMoreTab = true;
			}
			booleanTemp = true;
		}
		//jump to the next tag
		else {
			chevronArrayLength += arraySplitChevron[chevronOccurenceBeforeCursor].length;
			chevronOccurenceBeforeCursor++;
		}
	}
	return [cursorPositionToInsert, insertLineBefore, insertLineAfter, oneMoreTab];
}

/*------------------------------------------------------------------	
-----------------------------Statement------------------------------
-------------------------------------------------------------------*/

function insertStatement(statementType, pluginName, testName, pluginId) {
	var editor = ace.edit(getActiveEditor());
	var completeText = editor.getValue();
	var cache = localCache.get("pluginInfo");
	var xmlConverted = "";
	var retrievedJson = retrieveTest(pluginName, testName, cache);

	for (var i = 0; i < retrievedJson.length; i++){
		var temp = "<param name=\"" + retrievedJson[i] + "\" value=\'\'></param>" + "\n";
		xmlConverted += temp;
	}

	//build the xml String to insert
	var beginXml = "<" + statementType + ">";
	var endXml = "</" + statementType + ">"
	var beginCondition = "<condition use=\"" + pluginId + "\" name=\"" + testName + "\">";
	var endCondition = "</condition>"
	var additionalTag = "<then></then><else></else>";
	var beginParamsTag = "<params>";
	var endParamsTag = "</params>";

	var xml = beginXml + beginCondition;
	if (xmlConverted.length > 1) {
		xml += beginParamsTag + xmlConverted + endParamsTag;
	}
	xml += endCondition;
	if (statementType == "if") {
		xml += additionalTag;
	}
	xml += endXml;

	var answerPositionToInsertPrimitive = positionToInsert();
	var cursorPositionToInsert = answerPositionToInsertPrimitive[0];
	editor.moveCursorTo(cursorPositionToInsert[1], cursorPositionToInsert[0]);


	if (answerPositionToInsertPrimitive[1] == true) {
		xml = "\n" + xml;
	}
	var Range = ace.require('ace/range').Range;
	xml = formatXml(xml);
	//xml = addTabXml(xml, cursorPositionToInsert, completeText);
	xml = formatPreviousLine(xml, cursorPositionToInsert);
	editor.session.replace(new Range(cursorPositionToInsert[1], cursorPositionToInsert[0], cursorPositionToInsert[1], cursorPositionToInsert[0]), xml);
}


/**
* This function retrieves a given test. It reads the cache, retrieves the wanted JSON object and returns it.
* @param {String} pluginName : the plugin name
* @param {String} testName : the test name
* @param {Object} cache : the cache content
* @returns {String} the retrieved content of the primitive with an object format
*/
retrieveTest = function retrieveTest(pluginName, testName, cache) {
	var endFor = false;
	var retrievedJson;
	var i = 0;
	var j = 0;
	for (i; i < cache["plugins"].length && endFor == false; i++) {
		if (cache["plugins"][i]["name"] == pluginName) {
			for (j; j < cache["plugins"][i]["tests"].length && endFor == false; j++) {
				if (cache["plugins"][i]["tests"][j]["name"] == testName) {
					retrievedJson = cache["plugins"][i]["tests"][j]["params"];
					endFor = true;
				}
			}
		}
	}
	return retrievedJson;
}

function insertChoice() {
	var editor = ace.edit(getActiveEditor());
	var completeText = editor.getValue();
	var xml = "<nchoice>\n\t<choice proba=\"1\">\n\t</choice><choice proba=\"1\">\n\t</choice>\n</nchoice>";
	xml = formatXml(xml);
	var answerPositionToInsertPrimitive = positionToInsert();
	var cursorPositionToInsert = answerPositionToInsertPrimitive[0];
	//	xml = addTabXml(xml, cursorPositionToInsert, completeText);
	xml = formatPreviousLine(xml, cursorPositionToInsert);
	var Range = ace.require('ace/range').Range;
	editor.session.replace(new Range(cursorPositionToInsert[1], cursorPositionToInsert[0], cursorPositionToInsert[1], cursorPositionToInsert[0]), xml);

}
/*------------------------------------------------------------------	
-----------------------------Behavior-------------------------------
-------------------------------------------------------------------*/

function insertBehavior() {
	var edit = ace.edit(getActiveEditor());
	var completeText = edit.getValue();
	var behaviorId = chooseBehaviorId();
	var xmlBegin = "<behavior id=\"" + behaviorId + "\">";
	var xmlEnd = "</behavior>";
	var xml = xmlBegin + xmlEnd;
	var positionToInsert = positionInsertBehavior("</behaviors", completeText);
	//	xml = addTabXml(xml, positionToInsert, completeText);
	xml = formatPreviousLine(xml, positionToInsert, false, false);
	var completeXml = "\n" + xml;
	var Range = ace.require('ace/range').Range;
	edit.session.replace(new Range(positionToInsert[1], positionToInsert[0], positionToInsert[1], positionToInsert[0]), completeXml);
}

/**
 *This function is used to know where a behavior or a plugin tag must be inserted
 *@param {String} parentEndTag - a tag like /behaviors or /plugins
 *@param {String} completeText - what is written on the ace editor
 *@returns {Array} positionToInsert - An array of two elements:
 *- the column where the primitive will be inserted 
 *- the row where the primitive will be inserted
 */
function positionInsertBehavior(parentEndTag, completeText) {
	//number of character before reaching the cursor position
	var cursorColumn = 0;
	//counter
	var counterRowLine = 0;
	var counterRowChevron = 0;

	//Split the text twice: once to keep the cursor position, and once to browse the text  
	var arraySplitChevron = completeText.split(">");
	var arraySplitLine = completeText.split("\n");

	for (var i = 0; i < arraySplitChevron.length; i++) {
		if (arraySplitChevron[i].includes("<")) {
			arraySplitChevron[i] += ">";
		}
	}

	for (var i = 0; i < arraySplitLine.length; i++) {
		arraySplitLine[i] += "\n";
	}

	//retrieve the position of the parentEndTag
	var endTagReached = false;
	var i = 0;
	while (endTagReached == false) {
		if (arraySplitChevron[i].includes(parentEndTag)) {
			endTagReached = true;
		}
		else {
			counterRowChevron += arraySplitChevron[i].length;
			i++;
		}
	}

	//Converts this position to a valid one for the editor
	var line = 0;
	while (counterRowLine < counterRowChevron) {
		counterRowLine += arraySplitLine[line].length;
		line++;
	}

	cursorColumn = arraySplitLine[line - 1].length - (counterRowLine - counterRowChevron);
	var positionToInsert = [cursorColumn, line - 1]
	return positionToInsert;
}


function insertLoadprofile(behaviorName) {
	var edit = ace.edit(getActiveEditor());
	var completeText = edit.getValue();
	var groupBegin = "<group behavior=\"" + behaviorName + "\" forceStop=\"true\">" + "\n";
	var rampBegin = "\t" + "<ramp style=\"line\">" + "\n";
	var points1 = "\t\t" + "<points>" + "\n" + " \t\t\t<point x=\"0\" y=\"1\"></point>" + "\n";
	var points2 = "\t\t\t<point x=\"1\" y=\"1\"></point>" + "\n" + "\t\t</points>" + "\n";
	var rampEnd = "\t</ramp>\n";
	var groupEnd = "</group>";
	var xml = groupBegin + rampBegin + points1 + points2 + rampEnd + groupEnd;
	var positionToInsert = positionInsertBehavior("</loadprofile", completeText);
	//	xml = addTabXml(xml, positionToInsert, completeText);

	xml = formatPreviousLine(xml, positionToInsert, false, true);
	var completeXml = "\n" + xml;
	var Range = ace.require('ace/range').Range;
	edit.session.replace(new Range(positionToInsert[1], positionToInsert[0], positionToInsert[1], positionToInsert[0]), completeXml);
}
/*------------------------------------------------------------------	
------------------------------Shaping-------------------------------
-------------------------------------------------------------------*/

/** This function removes the Ace context menu
 */
function removeAceContextMenu() {
	contextMenuAceEditor.opacity = "0";
	contextMenuAceEditor.visibility = "hidden";
}

/** 
* This function formats the retrieved plugin.xml content. For that, it checks the previous line
* @param {String}lineContent - The editor's line containing </plugin>
* @param {String}pluginContent - The text that was retrieved from the plugin.xml file
*/
function formatPluginInsertion(lineContent, pluginContent) {
	var formattedPluginContent = pluginContent;
	var tabCount = (lineContent.match(/\t/g) || []).length;
	var tabToInsert = tabCount + 1;
	var insertedCharacter = 0;

	//insert the tabs at beginning
	for (var i = 0; i < tabToInsert; i++) {
		formattedPluginContent = "\t" + formattedPluginContent;
		insertedCharacter += 1;
	}

	//format plugin.xml content
	var pluginCharIndex = pluginContent.indexOf("\n");
	while (pluginCharIndex != -1) {
		var count = 0;
		while (count < tabToInsert) {
			formattedPluginContent = formattedPluginContent.slice(0, pluginCharIndex + 1 + insertedCharacter)
				+ "\t" + formattedPluginContent.slice(pluginCharIndex + 1 + insertedCharacter);
			count++;
			insertedCharacter += 1;
		}
		pluginCharIndex = pluginContent.indexOf("\n", pluginCharIndex + 1);
	}

	//format plugin insertion
	var index = lineContent.indexOf('</plugins>');
	var firstHalf = lineContent.slice(0, index);
	var secondHalf = lineContent.slice(index);
	var newLine = firstHalf.concat('\n', formattedPluginContent);
	newLine = newLine.concat('\n\t\t', secondHalf);
	return newLine;
}

function editorFormatXml() {
	var editor = ace.edit(getActiveEditor());
	var xmlEditorContent = editor.getValue();
	var formattedXml = formatXml(xmlEditorContent);
	editor.setValue(formattedXml);
}

/**
 *This function adds tabs to a formated xml String by checking the xml tags
 *@param {String} xml - The formatted xml String
 *@param {Array} cursorPositionInsert - The row and column where the xml must be inserted
 *@returns {String} formatedXml - The xml with the inserted tabulation 
 */
addTabXml = function addTabXml(xml, cursorPositionToInsert, completeText) {
	var formattedXml = "";
	//split the xml by its line break
	var arrayXml = xml.split("\n");
	for (var i = 0; i < arrayXml.length - 1; i++) {
		arrayXml[i] += "\n";
	}

	//split the text by its >
	var arrayChevron = completeText.split(">");
	for (var i = 0; i < arrayChevron.length - 1; i++) {
		arrayChevron[i] += ">";
	}

	//obtain the absolute position of the cursor
	var cursorAbsolutePosition = 0;
	var arrayCompleteText = completeText.split("\n");
	for (var i = 0; i < arrayCompleteText.length; i++) {
		arrayCompleteText[i] += "\n";
	}
	for (var i = 0; i < cursorPositionToInsert[1]; i++) {
		cursorAbsolutePosition += arrayCompleteText[i].length;
	}
	cursorAbsolutePosition += cursorPositionToInsert[0];

	//Browse arrayChevron to determine the number of tab that need to be inserted
	var tabNumber = 0;
	var arrayChevronRow = 0;
	var arrayChevronLength = 0;
	while (arrayChevronLength <= cursorAbsolutePosition) {
		if (arrayChevron[arrayChevronRow].includes("<scenario>") ||
			arrayChevron[arrayChevronRow].includes("<behaviors") ||
			arrayChevron[arrayChevronRow].includes("<behavior") ||
			arrayChevron[arrayChevronRow].includes("<if") ||
			arrayChevron[arrayChevronRow].includes("<while") ||
			arrayChevron[arrayChevronRow].includes("<then") ||
			arrayChevron[arrayChevronRow].includes("<else") ||
			arrayChevron[arrayChevronRow].includes("<preemptive")) {
			tabNumber++;
		}
		if (arrayChevronRow > 1) {
			if (arrayChevron[arrayChevronRow - 1].includes("</scenario>") ||
				arrayChevron[arrayChevronRow - 1].includes("</behaviors") ||
				arrayChevron[arrayChevronRow - 1].includes("</behavior") ||
				arrayChevron[arrayChevronRow - 1].includes("</if") ||
				arrayChevron[arrayChevronRow - 1].includes("</while") ||
				arrayChevron[arrayChevronRow - 1].includes("</then") ||
				arrayChevron[arrayChevronRow - 1].includes("</else") ||
				arrayChevron[arrayChevronRow - 1].includes("</preemptive")) {
				tabNumber--;
			}
		}

		arrayChevronLength += arrayChevron[arrayChevronRow].length;
		arrayChevronRow++;
	}

	//add tabs to each line
	for (var i = 0; i < arrayXml.length; i++) {
		for (var j = 0; j < tabNumber; j++) {
			arrayXml[i] = "\t" + arrayXml[i];
		}
		formattedXml += arrayXml[i];
	}
	return formattedXml;
}

/**
 *This function adds tabs to a formated xml String by checking the previous xml tags
 *@param {String} xml - The formatted xml String
 *@param {Array} cursorPositionInsert - The row and column where the xml must be inserted
 *@returns {String} formatedXml - The xml with the inserted tabulation 
 */
function formatPreviousLine(xml, cursorPositionToInsert, insertLineAfter, oneMoreTab) {
	var formattedXml = "";
	//split the xml by its line break
	var editor = ace.edit(getActiveEditor());
	var completeText = editor.getValue();

	if (insertLineAfter == true) {
		xml += "\n";
	}

	var arrayXml = xml.split("\n");
	for (var i = 0; i < arrayXml.length - 1; i++) {
		arrayXml[i] += "\n";
	}

	//obtain the absolute position of the cursor
	var cursorAbsolutePosition = 0;
	var arrayCompleteText = completeText.split("\n");
	for (var i = 0; i < arrayCompleteText.length; i++) {
		arrayCompleteText[i] += "\n";
	}
	for (var i = 0; i < cursorPositionToInsert[1]; i++) {
		cursorAbsolutePosition += arrayCompleteText[i].length;
	}
	cursorAbsolutePosition += cursorPositionToInsert[0];

	var stopTabCount = false;
	var charIndex = 0;
	var spaceCount = 0;
	var tabCount = 0;
	while (stopTabCount != true && charIndex < arrayCompleteText[cursorPositionToInsert[1]].length) {
		if (arrayCompleteText[cursorPositionToInsert[1]][charIndex] == "\t") {
			tabCount++;
			spaceCount = 0;
		}
		else if (arrayCompleteText[cursorPositionToInsert[1]][charIndex] == " ") {
			spaceCount++;
		}
		else {
			stopTabCount = true;
		}
		if (spaceCount == 4) {
			spaceCount = 0;
			tabCount++
		}
		charIndex++;
	}

	if (oneMoreTab == true) {
		tabCount++;
	}

	//add tabs to each line
	for (var i = 0; i < arrayXml.length; i++) {
		for (var j = 0; j < tabCount; j++) {
			arrayXml[i] = "\t" + arrayXml[i];
		}
		formattedXml += arrayXml[i];
	}
	return formattedXml;
}

/*------------------------------------------------------------------	
------------------------------File Name-----------------------------
-------------------------------------------------------------------*/

/**
*Check if the file's name contains .extension or not
*If the file doesn't contains the extension at its end, it is added
*@param {string} extension - The extension of the file
*@param {string} filePromptName - The file's name
*@returns {string} The new file's name
 */
checkFilePromptName = function checkFilePromptName(extension, filePromptName) {
	var extensionIndex = filePromptName.lastIndexOf("." + extension);
	if (extensionIndex == -1) {
		filePromptName = filePromptName + "." + extension;
	}
	else if ((filePromptName.length - extensionIndex) != 4) {
		filePromptName = filePromptName + "." + extension;
	}
	return filePromptName;
}

/**
 *This function is called when a ctp file is created from an xis file. It adds the xis file name as a blade argument
 */
function addFileNameTestPlan() {
	var editor = ace.edit(getActiveEditor());
	var completeText = editor.getValue();
	var xisFileName = localCache.get("clickedXisFile");
	var arrayXml = completeText.split("\n");
	for (var i = 0; i < arrayXml.length; i++) {
		if (arrayXml[i].includes("blade.1.argument=")) {
			arrayXml[i] += xisFileName;
		}
	}
	completeText = "";
	for (var i = 0; i < arrayXml.length - 1; i++) {
		completeText += arrayXml[i] + "\n";
	}
	editor.setValue(completeText);
}

module.exports = { retrievePrimitive, checkFilePromptName, addTabXml };

