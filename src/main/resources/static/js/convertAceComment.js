/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2021 Antoine Thevenet
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

/**
 * The following functions manipulate Yaml ans Xml comments so that they can be  
 * tranformed into comments from the other language.
 */

//Global variable that contain xml comments located outside of <scenario> tag
xmlBeginComment = [];
xmlEndComment = [];


/**
 * Retrieve a comment in xml and transform it to a xml tag
 * Thanks to it, xml comments can be incorporated in the yaml editor
 */
function XMLCommentToTag(xml) {
	//	remove the empty line and text that are before and after the xml
	while (xml[0] != "<") {
		xml = xml.slice(1);
	}
	while (xml[xml.length - 1] != ">") {
		xml = xml.slice(0, xml.length - 1);
	}
	commentCount = 0;
	var xmlArray = xml.slice(1, -1).split(/>\s*</);
	//retrieve the scenario and /scenario tag
	var beginLine = 0;
	var endLine = 0;
	for (var i = 0; i < xmlArray.length; i++) {
		if (xmlArray[i] == "scenario") {
			beginLine = i;
		}
		else if (xmlArray[i] == "/scenario") {
			endLine = i;
		}
	}
	//transform comment between <scenario> and </scenario>
	for (var i = beginLine; i < endLine; i++) {
		var xmlRow = xmlArray[i];
		//the first character is !, which is a comment
		if (xmlRow[0] == '!' && xmlRow[1] == "-") {
			//Check where the comment ends
			//if the comment ends at a following line, transform the line in a unique comment
			var tempCount = i;
			while (xmlRow[xmlRow.length - 1] != "-" && xmlRow[xmlRow.length - 2] != "--"){
				xmlArray[tempCount] = xmlArray[tempCount] + "--";
				xmlArray[tempCount + 1] = "!--" + xmlArray[tempCount + 1];
				tempCount ++;
				xmlRow = xmlArray[tempCount];
			}
			xmlRow = xmlArray[i];
			
			//retrieve the next line without comment
			var nextLineNoComment = i + 1;
			while(xmlArray[nextLineNoComment][0] == "!"){
				nextLineNoComment ++;
			}
			//comment for a use tag
			if (xmlArray[nextLineNoComment].includes("use id")) {
				var comment = xmlRow.substring(3, xmlRow.length - 2);
				comment = comment.replace(/\"/g, "_%%");
				var commentToXmlTag = "use id=\"" + "_IsacXmlUseComment" + commentCount + "\"" + " value=\"" + comment + "\"";
				xmlArray[i] = commentToXmlTag;
				xmlArray.splice(nextLineNoComment, 0, "/use");
				commentCount++;
			}
			else if (xmlArray[nextLineNoComment].includes("param name")) {
				var comment = xmlRow.substring(3, xmlRow.length - 2);
				comment = comment.replace(/\"/g, "_%%");
				var commentToXmlTag = "param id=\"" + "_IsacXmlParamComment" + commentCount + "\"" + " value=\"" + comment + "\"";
				xmlArray[i] = commentToXmlTag;
				xmlArray.splice(nextLineNoComment, 0, "/param");
				commentCount++;
			}
			//choice comment
			else if (xmlArray[nextLineNoComment].includes("choice proba")) {
				var comment = xmlRow.substring(3, xmlRow.length - 2);
				comment = comment.replace(/\"/g, "_%%");
				var commentToXmlTag = "choice proba=\"" + "_IsacXmlChoiceComment" + commentCount + "$$$$" + comment + "\"";
				xmlArray[i] = commentToXmlTag;
				xmlArray.splice(nextLineNoComment, 0, "/choice");
				commentCount++;
			}
			//comment for behaviors, plugins, params, loadprofile, then, else
			else if (xmlArray[nextLineNoComment] == "behaviors" || xmlArray[nextLineNoComment] == "plugins" || xmlArray[nextLineNoComment] == "loadprofile" ||
			xmlArray[nextLineNoComment] == "params" || xmlArray[nextLineNoComment] == "then" || xmlArray[nextLineNoComment] == "else") {
				var comment = xmlRow.substring(3, xmlRow.length - 2);
				comment = comment.replace(/\"/g, "_%%");
				var commentToXmlTag = "_IsacXmlSpaceComment" + commentCount + " value=\"" + comment + "\"";
				xmlArray[i] = commentToXmlTag;
				xmlArray.splice(nextLineNoComment, 0, "/_IsacXmlSpaceComment" + commentCount);
				commentCount++;
			}
			//normal comment
			else {
				//retrieve the comment and change it to a tag
				var comment = xmlRow.substring(3, xmlRow.length - 2);
				comment = comment.replace(/\"/g, "_%%");
				var commentToXmlTag = "_IsacXmlComment" + commentCount + " value=\"" + comment + "\"";
				xmlArray[i] = commentToXmlTag;
				xmlArray.splice(nextLineNoComment, 0, "/_IsacXmlComment" + commentCount);
				commentCount++;
			}
			endLine ++;
		}
	}
	var newXmlText = "";
	//recreate the xml
	for (var i = 0; i < xmlArray.length; i++) {
		newXmlText += "<" + xmlArray[i] + ">\n";
	}
	//
	retrieveXmlCommentOutScenario(beginLine, endLine, xmlArray);
	return newXmlText;
}

/**
 * Retrieve a comment in Yaml and transform it to a Yaml tag
 * Thanks to it, yaml comments can be incorporated in the xml editor
 */
function YAMLCommentToTag(yaml) {
	var yamlArray = yaml.split("\n");
	var commentCount = 0;
	//delete empty lines
	for (var i = 0; i < yamlArray.length; i++) {
		if (yamlArray[i] == "") {
			yamlArray.splice(i, 1);
		}
	}
	for (var i = 0; i < yamlArray.length; i++) {
		var lineToTest = yamlArray[i];
		//find the comment
		if (lineToTest.includes("#")) {
			//position of the comment
			var commentPos = yamlArray[i].indexOf("#");
			var commentContent = yamlArray[i].substring(commentPos + 1);
			var commentSpaceNumber = 0;
			var j = 0;
			while (lineToTest[j] == " ") {
				commentSpaceNumber++;
				j++;
			}
			//retrieve the number of space from the previous line
			var k = i + 1;
			var stop = false;
			var spaceToInsert = 0;
			while (stop == false) {
				//last line
				if (k >= yamlArray.length) {
					stop = true;
					spaceToInsert = 0;
				}
				//another comment
				else if (yamlArray[k].includes("#")) {
					k++;
				}
				//yaml line
				else {
					var l = 0;
					while (yamlArray[k][l] == " ") {
						spaceToInsert++;
						l++;
					}
					stop = true;
				}
			}
			// transform the comment to Yaml
			//control, sample, timer
			if (k < yamlArray.length && (yamlArray[k].includes("- control") || yamlArray[k].includes("- timer")
				|| yamlArray[k].includes("- sample") || yamlArray[k].includes("- if")
				|| yamlArray[k].includes("- while") || yamlArray[k].includes("- preemptive"))) {
				//retrieve the tag after "-". Example : id/control/sample
				var tagWord = yamlArray[k].substring(yamlArray[k].indexOf("-") + 2, yamlArray[k].indexOf(":"))
				var firstLine = "- " + tagWord + ":";
				var secondLine = "    use: " + "_YamlUseComment" + commentCount;
				var thirdLine = "    value: " + commentContent;
				for (var j = 0; j < spaceToInsert; j++) {
					firstLine = " " + firstLine;
					secondLine = " " + secondLine;
					thirdLine = " " + thirdLine;
				}
				var yamlComment = firstLine + "\n" + secondLine + "\n" + thirdLine;
			}
			//nchoice
			else if (k < yamlArray.length && yamlArray[k].includes("- nchoice")) {
				//retrieve the tag after "-". Example : id/control/sample
				var firstLine = "- timer:";
				var secondLine = "    use: " + "_YamlUseComment" + commentCount;
				var thirdLine = "    value: " + commentContent;
				for (var j = 0; j < spaceToInsert; j++) {
					firstLine = " " + firstLine;
					secondLine = " " + secondLine;
					thirdLine = " " + thirdLine;
				}
				var yamlComment = firstLine + "\n" + secondLine + "\n" + thirdLine;
			}
			//choice
			else if (k < yamlArray.length && yamlArray[k].includes("- proba")) {
				//retrieve the tag after "-". Example : id/control/sample

				var yamlComment = "- " + "proba: _YamlChoiceComment$$$$" + commentContent;
				for (var j = 0; j < spaceToInsert; j++) {
					yamlComment = " " + yamlComment;
				}
			}
			//use and param comment
			else if (k < yamlArray.length && yamlArray[k].includes("-")) {
				//retrieve the tag after "-". Example : id/control/sample
				var tagWord = yamlArray[k].substring(yamlArray[k].indexOf("-") + 2, yamlArray[k].indexOf(":"))
				var firstLine = "- " + tagWord + ": _YamlUseComment" + commentCount;
				var secondLine = "  value: " + commentContent;
				for (var j = 0; j < spaceToInsert; j++) {
					firstLine = " " + firstLine;
					secondLine = " " + secondLine;
				}
				var yamlComment = firstLine + "\n" + secondLine;
			}
			//normal comment
			else {
				var firstLine = "_YamlComment" + commentCount + ": ";
				var secondLine = "  value: " + commentContent;
				for (var j = 0; j < spaceToInsert; j++) {
					firstLine = " " + firstLine;
					secondLine = " " + secondLine;
				}
				var yamlComment = firstLine + "\n" + secondLine;
			}


			//If the number of space is equal to the # location, the comment is inserted at the current line
			//Changes where the comment will be inserted
			if (commentSpaceNumber == commentPos) {
				yamlArray[i] = yamlComment;
			}
			//Else, it is inserted to the next line
			else {
				yamlArray.splice(i + 1, 0, yamlComment);
			}
			commentCount++;
		}
	}
	var returnYaml = "";
	for (var i = 0; i < yamlArray.length; i++) {
		returnYaml += yamlArray[i] + "\n";
	}
	return returnYaml;
}

/**
 * Browse the yaml editor. If a specific term such as _IsacXmlComment is found, transform it to a yaml comment.
 * @param {String} yamlText - the text from the Yaml editor
 */
function transformYamlTagToComment(yamlText) {
	var yamlTextArray = yamlText.split("\n");
	for (var i = 0; i < yamlTextArray.length; i++) {
		//normal comment
		if (yamlTextArray[i].includes("_IsacXmlComment")) {
			//retrieve the comment text at the next row
			var xmlComment = yamlTextArray[i + 1];
			//keep only what is after :
			xmlComment = "#" + xmlComment.substring(xmlComment.indexOf(":") + 2);
			xmlComment = xmlComment.replace(/_%%/g, "\"");
			//change the line and remove the next one
			var spaceNumber = yamlTextArray[i].indexOf("_IsacXml");
			for (var j = 0; j < spaceNumber - 2; j++) {
				xmlComment = " " + xmlComment;
			}
			yamlTextArray[i] = xmlComment;
			yamlTextArray.splice(i + 1, 1);
		}
		//normal comment with different spaceNumber
		else if (yamlTextArray[i].includes("_IsacXmlSpaceComment")) {
			//retrieve the comment text at the next row
			var xmlComment = yamlTextArray[i + 1];
			//keep only what is after :
			xmlComment = "#" + xmlComment.substring(xmlComment.indexOf(":") + 2);
			xmlComment = xmlComment.replace(/_%%/g, "\"");
			//change the line and remove the next one
			var spaceNumber = yamlTextArray[i].indexOf("_IsacXml");
			for (var j = 0; j < spaceNumber; j++) {
				xmlComment = " " + xmlComment;
			}
			yamlTextArray[i] = xmlComment;
			yamlTextArray.splice(i + 1, 1);
		}
		//choice comment
		else if (yamlTextArray[i].includes("_IsacXmlChoiceComment")) {
			//retrieve the comment text at the next row
			var xmlComment = yamlTextArray[i];
			//keep only what is after :
			xmlComment = "#" + xmlComment.substring(xmlComment.indexOf("$$$$") + 4);
			xmlComment = xmlComment.replace(/_%%/g, "\"");
			//change the line and remove the next one
			var spaceNumber = yamlTextArray[i].indexOf("-");
			for (var j = 0; j < spaceNumber; j++) {
				xmlComment = " " + xmlComment;
			}
			yamlTextArray[i] = xmlComment;
		}
		//-id or -name comment 
		else if (yamlTextArray[i].includes("_IsacXmlUseComment") || yamlTextArray[i].includes("_IsacXmlParamComment")) {
			//retrieve the comment text at the next row
			var xmlComment = yamlTextArray[i + 1];
			//keep only what is after :
			xmlComment = "#" + xmlComment.substring(xmlComment.indexOf(":") + 2);
			xmlComment = xmlComment.replace(/_%%/g, "\"");
			//change the line and remove the next one
			var spaceNumber = yamlTextArray[i].indexOf("-");
			for (var j = 0; j < spaceNumber; j++) {
				xmlComment = " " + xmlComment;
			}
			yamlTextArray[i] = xmlComment;
			yamlTextArray.splice(i + 1, 1);
		}
	}
	var newYaml = "";
	for (var i = 0; i < yamlTextArray.length; i++) {
		newYaml += yamlTextArray[i] + "\n";
	}
	return newYaml;
}

/**
 * Browse the xml editor. If a specific tag such as _YamlComment is found, transforms the xml tag to a xml comment
 * @param {String} xmlText - the text from the xml editor
 */
function transformXmlTagToComment(xmlText) {
	var xmlTextArray = xmlText.split("\n");
	//delete empty lines
	for (var i = 0; i < xmlTextArray.length; i++) {
		if (xmlTextArray[i] == "") {
			xmlTextArray.splice(i, 1);
		}
	}
	for (var i = 0; i < xmlTextArray.length; i++) {
		if (xmlTextArray[i].includes("_YamlComment")) {
			//retrieve the comment text
			var yamlComment = xmlTextArray[i];
			//keep only what is after :
			var yamlComment = "<!--" + yamlComment.substring(yamlComment.indexOf("=") + 2, yamlComment.indexOf(">") - 1) + "-->";
			//change the line and remove the next one
			tabNumber = xmlTextArray[i].indexOf("<");
			for (var j = 0; j < tabNumber; j++) {
				yamlComment = "\t" + yamlComment;
			}
			xmlTextArray[i] = yamlComment;
		}

		if (xmlTextArray[i].includes("_YamlUseComment")) {
			//retrieve the comment text
			var yamlComment = xmlTextArray[i];
			//keep only what is after :
			var yamlComment = "<!--" + yamlComment.substring(yamlComment.indexOf("value=") + 7, yamlComment.indexOf(">") - 1) + "-->";
			//change the line and remove the next one
			tabNumber = xmlTextArray[i].indexOf("<");
			for (var j = 0; j < tabNumber; j++) {
				yamlComment = "\t" + yamlComment;
			}
			xmlTextArray[i] = yamlComment;
		}
		//choice comment
		if (xmlTextArray[i].includes("_YamlChoiceComment")) {
			//retrieve the comment text
			var yamlComment = xmlTextArray[i];
			//keep only what is after :
			var yamlComment = "<!--" + yamlComment.substring(yamlComment.indexOf("$$$$") + 4, yamlComment.indexOf(">") - 1) + "-->";
			//change the line and remove the next one
			tabNumber = xmlTextArray[i].indexOf("<");
			for (var j = 0; j < tabNumber; j++) {
				yamlComment = "\t" + yamlComment;
			}
			xmlTextArray[i] = yamlComment;
		}
	}
	var newXml = "";
	for (var i = 0; i < xmlTextArray.length; i++) {
		newXml += xmlTextArray[i] + "\n";
	}
	return newXml;
}

/**
 * Insert comments retrieved from the global varial 
 * xmlBeginComment xmlEndComment in the yaml editor
 * @param {String} yaml - The yaml editor content
 */
function topBotXmlCommentToYaml(yaml) {
	for (var i = 0; i < xmlBeginComment.length; i++) {
		yaml = "#" + xmlBeginComment[i] + "\n" + yaml;
	}
	for (var i = 0; i < xmlEndComment.length; i++) {
		yaml += "\n" + "#" + xmlEndComment[i];
	}
	return yaml;
}


/**
 * Retrieve the comments that are located outside of the scenario tag from the xml editor
 * @param {number} beginLine - The line of the tag <scenario>
 * @param {number} endLine - The line of the tag </scenario
 * @param {Array} xmlArray - The array containing all the editor text. 
 */
function retrieveXmlCommentOutScenario(beginLine, endLine, xmlArray) {
	//add the comment before <scenario> and </scenario> to a variable global
	xmlBeginComment = [];
	xmlEndComment = [];
	for (var i = 0; i < beginLine; i++) {
		var xmlRow = xmlArray[i];
		if (xmlRow[0] == '!' && xmlRow[1] == "-") {
			//retrieve the comment and change it to a tag
			var comment = xmlRow.substring(3, xmlRow.length - 2);
			xmlBeginComment.push(comment);
		}
	}
	for (var i = endLine; i < xmlArray.length; i++) {
		var xmlRow = xmlArray[i];
		if (xmlRow[0] == '!' && xmlRow[1] == "-") {
			//retrieve the comment and change it to a tag
			var comment = xmlRow.substring(3, xmlRow.length - 2);
			xmlEndComment.push(comment);
		}
	}
}