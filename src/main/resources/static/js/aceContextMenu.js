/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2021 Antoine Thevenet
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

/**
 *This function access the local cache in order to add a list of plugins and primitives to the
 *Ace context menu
 */

/*------------------------------------------------------------------	
---------------------Ace Context menu handler-----------------------
-------------------------------------------------------------------*/

function aceContextMenuHandler() {
	removeAceContextMenu();
	clearContextMenu();
	var arrayActivePlugins = scanPlugins();
	var uniqueActivePlugin = arrayActivePlugins.filter(getUniqueVal);

	var booleanDisablePrimitive = disableMenuItems();

	if (booleanDisablePrimitive == false) {
		addStatementToAceContextMenu("NewStatementMenu");
		addStatementToAceContextMenu("parentControlSubmenu");
		addStatementToAceContextMenu("parentTimerSubmenu");
		addStatementToAceContextMenu("parentSampleSubmenu");
	}

}

/**
 *This function browses the editor to determine where the user right-cliked.
 *If the user didn't'right clicked between the tags <behavior and <behavior>, the return value 
 *will disable some menu options
 *
 *@return {boolean} disable - If disable == true, some menu options will be disabled
 */
function disableMenuItems() {
	var editor = ace.edit(getActiveEditor());
	var cursorPosition = editor.selection.getCursor();
	var completeText = editor.getValue();
	var cursorAbsolutePosition = 0;
	var disable = true;

	//obtain the cursor absolute position
	var arrayCompleteText = completeText.split("\n");
	for (var i = 0; i < arrayCompleteText.length - 1; i++) {
		arrayCompleteText[i] += "\n";
	}
	for (var counterRowLine = 0; counterRowLine < cursorPosition["row"]; counterRowLine++) {
		cursorAbsolutePosition += arrayCompleteText[counterRowLine].length;
	}
	cursorAbsolutePosition += cursorPosition["column"];

	//add to an array the <behavior and </behavior> positions
	//the first value of behaviorBeginTagArray is shifted as the <behaviors> tag is taken into account
	var behaviorBeginTagArray = getMatchIndexes(completeText, "<behavior");
	var behaviorEndTagArray = getMatchIndexes(completeText, "</behavior>");

	behaviorBeginTagArray.shift();

	for (var i = 0; i < behaviorBeginTagArray.length; i++) {
		if (cursorAbsolutePosition >= behaviorBeginTagArray[i] && cursorAbsolutePosition <= behaviorEndTagArray[i] + "</behavior>".length) {
			disable = false;
		}
	}
	return disable;

}
/**
 *This function is used to obtain the indexes of substring inside a string
 *@param {String} str - The input String
 *@param {String} toMatch - The substring that we are looking for
 *@return {array} indexMatches - An array with the position of the different indexMatches
 */
function getMatchIndexes(str, toMatch) {
	var toMatchLength = toMatch.length,
		indexMatches = [], match,
		i = 0;

	while ((match = str.indexOf(toMatch, i)) > -1) {
		indexMatches.push(match);
		i = match + toMatchLength;
	}

	return indexMatches;
}

function getUniqueVal(value, index, self) {
	return self.indexOf(value) === index;
}

/**
 *This function returns an array that contains the plugins names contained inside the cache
 */
function getAvailablePlugin() {
	var arrayAvailablePlugin = [];
	var cache = localCache.get("pluginInfo");
	var pluginNumber = cache["plugins"].length;
	for (var i = 0; i < pluginNumber; i++) {
		arrayAvailablePlugin.push(cache["plugins"][i]["name"]);
	}
	return arrayAvailablePlugin;
}


function addStatementToAceContextMenu(menuId) {
	var statementSubmenu = document.getElementById(menuId);
	if (statementSubmenu.classList.contains("noHover") == true) {
		statementSubmenu.classList.remove("noHover");
	}
}


/*------------------------------------------------------------------	
------------------------------Clear---------------------------------
-------------------------------------------------------------------*/

/**
 *This function clears the control, the timer, the sample and plugin submenuChild.
 *It is used to avoid having the same primitive displayed twice.
 *It adds the style class noHover to the statement submenu. This style class prevents the menu display
 */
function clearContextMenu() {
	var controlSubmenu = document.getElementById("parentControlSubmenu");
	var timerSubmenu = document.getElementById("parentTimerSubmenu");
	var sampleSubmenu = document.getElementById("parentSampleSubmenu");
	var statementSubmenu = document.getElementById("NewStatementMenu");

	if (controlSubmenu.classList.contains("noHover") == false) {
		controlSubmenu.classList.add("noHover");
	}
	if (timerSubmenu.classList.contains("noHover") == false) {
		timerSubmenu.classList.add("noHover");
	}
	if (sampleSubmenu.classList.contains("noHover") == false) {
		sampleSubmenu.classList.add("noHover");
	}
	if (statementSubmenu.classList.contains("noHover") == false) {
		statementSubmenu.classList.add("noHover");
	}


}


/*------------------------------------------------------------------	
-------------------------Scan Ace Editor----------------------------
-------------------------------------------------------------------*/

/**
 *This function scans the xml ace editor to retrieve the plugins used for the current xis file
@returns {array} An array that contains the used plugins
 */
function scanPlugins() {
	var editor = ace.edit(getActiveEditor());
	var textToAnalyse = editor.getValue();
	var arrayActivePlugins = [];
	var useTagContent;
	var tmpPluginName;

	if (textToAnalyse.indexOf("<plugins>") != -1 && textToAnalyse.indexOf("</plugins>") != -1) {
		textToAnalyse = textToAnalyse.substring(textToAnalyse.indexOf("<plugins>"), textToAnalyse.indexOf("</plugins>"));

		var indexUseTag = textToAnalyse.indexOf("<use");
		var indexUseEndTag = textToAnalyse.indexOf("</use>");

		while (indexUseTag != -1) {
			useTagContent = textToAnalyse.substring(indexUseTag, indexUseEndTag);
			tmpPluginName = useTagContent.substring(useTagContent.indexOf("name"), useTagContent.indexOf(">"));
			tmpPluginName = tmpPluginName.replaceAll('\"', '');
			tmpPluginName = tmpPluginName.replaceAll('name', '');
			tmpPluginName = tmpPluginName.replaceAll(' ', '');
			tmpPluginName = tmpPluginName.replaceAll('=', '');
			arrayActivePlugins.push(tmpPluginName);
			indexUseTag = textToAnalyse.indexOf("<use", indexUseTag + 1);
			indexUseEndTag = textToAnalyse.indexOf("</use>", indexUseEndTag + 1);
		}
	}
	return arrayActivePlugins;
}

/**
 * This function scans the ace editor content and retrieve the ids of a specific plugin
 */
function scanPluginId(pluginName) {
	var arrayPluginId = [];
	var textAsObject = stringToObj();
	if (textAsObject.hasOwnProperty("behaviors")) {
		if (textAsObject["behaviors"]["plugins"].hasOwnProperty("use")) {
			if (textAsObject["behaviors"]["plugins"]["use"].length == undefined) {
				if (textAsObject["behaviors"]["plugins"]["use"]["name"] == pluginName) {
					arrayPluginId.push(textAsObject["behaviors"]["plugins"]["use"]["id"]);
				}
			}
			else {
				var useCount = textAsObject["behaviors"]["plugins"]["use"].length;
				for (var i = 0; i < useCount; i++) {
					if (textAsObject["behaviors"]["plugins"]["use"][i]["name"] == pluginName) {
						arrayPluginId.push(textAsObject["behaviors"]["plugins"]["use"][i]["id"]);
					}
				}
			}

		}
	}

	return arrayPluginId;
}

/**
 * This function scans the ace editor content and defines the default ID that will be assigned to the tag <use>
 */
function choosePluginId(pluginName) {

	var arrayPluginId = scanPluginId(pluginName);
	var extension = 0;
	var i = 0;
	var nameToTest = pluginName + "_" + extension;
	while (i < arrayPluginId.length) {
		nameToTest = pluginName + "_" + extension;
		if (nameToTest == arrayPluginId[i]) {
			i = 0;
			extension++;
		}
		else {
			i++;
		}
	}
	return nameToTest;
}

/**
 * This function scans the ace editor content and retrieve the ids of each behavior
 */
function scanBehaviorId() {
	var arrayBehaviorId = [];
	var textAsObject = stringToObj();
	if (textAsObject["behaviors"].hasOwnProperty("behavior")) {
		var useCount = textAsObject["behaviors"]["behavior"].length;
		if (useCount != undefined) {
			for (var i = 0; i < useCount; i++) {
				arrayBehaviorId.push(textAsObject["behaviors"]["behavior"][i]["id"]);
			}
		}
		else {
			arrayBehaviorId.push(textAsObject["behaviors"]["behavior"]["id"])
		}
	}

	return arrayBehaviorId;
}

/**
 * This function scans the ace editor content and defines the default ID that will be assigned to the tag <behavior>
 */
function chooseBehaviorId() {

	var arrayBehaviorId = scanBehaviorId();
	var extension = 0;
	var i = 0;
	var behaviorId = "B" + extension;
	while (i < arrayBehaviorId.length) {
		behaviorId = "B" + extension;
		if (behaviorId == arrayBehaviorId[i]) {
			i = 0;
			extension++;
		}
		else {
			i++;
		}
	}
	return behaviorId;
}
