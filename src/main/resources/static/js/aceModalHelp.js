/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2021 Antoine Thevenet
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

/*------------------------------------------------------------------	
--------------------------------Test--------------------------------
-------------------------------------------------------------------*/
function displayHelp() {
	var helpWindow = document.getElementById("modalHelp");
	helpWindow.removeAttribute("hidden");

	clearModalHelp();

	var pluginName = document.getElementById("statementPluginNameSelect");
	var testName = document.getElementById("statementTestSelect");
	if (pluginName.options.length > 0 && testName.options.length > 0) {
		var stringPluginName = pluginName.options[pluginName.selectedIndex].text;
		var stringTestName = testName.options[testName.selectedIndex].text;
		var textHelp = retrieveTestHelp(stringPluginName, stringTestName);
		if (textHelp == undefined || textHelp == "") {
			textHelp = getLocalizedText("error.modalHelp.noDescription");
		}

		//add test and plugin name
		var modalHelpTitle = document.getElementById("modalHelpTitle");
		var textHelpTitle = document.createTextNode(stringPluginName + "." + stringTestName);
		modalHelpTitle.appendChild(textHelpTitle);

		//add text to the modal body
		var textParentNode = document.getElementById("textHelp");
		var span = document.createElement("span");
		var textAsNode = document.createTextNode(textHelp);

		span.appendChild(textAsNode);
		textParentNode.appendChild(span);

	}
	else {
		var errorText = getLocalizedText("error.modalHelpTest.noSelection");
		var textParentNode = document.getElementById("textHelp");
		var textAsNode = document.createTextNode(errorText);
		var span = document.createElement("span");
		span.appendChild(textAsNode);
		textParentNode.appendChild(span);
	}
}

function retrieveTestHelp(pluginName, testName) {

	var cache = localCache.get("pluginInfo");
	var pluginNumber = cache["plugins"].length;
	var testHelp;

	for (var i = 0; i < pluginNumber; i++) {
		if (pluginName == cache["plugins"][i]["name"]) {
			if (cache["plugins"][i].hasOwnProperty("tests")) {
				for (var j = 0; j < cache["plugins"][i]["tests"].length; j++) {
					if (cache["plugins"][i]["tests"][j]["name"] == testName) {
						testHelp = cache["plugins"][i]["tests"][j]["help"];
					}
				}
			}
		}
	}
	return testHelp;
}

/*------------------------------------------------------------------	
------------------------------Primitive-----------------------------
-------------------------------------------------------------------*/
function displayHelpPrimitive() {
	var helpWindow = document.getElementById("modalHelp");
	helpWindow.removeAttribute("hidden");
	clearModalHelp();

	var primitiveName = document.getElementById("primitivePrimitiveNameSelect");
	var pluginName = document.getElementById("primitivePluginNameSelect");
	if (primitiveName.options.length > 0) {
		var stringPrimitiveName = primitiveName.options[primitiveName.selectedIndex].text;
		var stringPluginName = pluginName.options[pluginName.selectedIndex].text;
		var primitiveType = localCache.get("modalPrimitiveType");
		var textHelp = retrievePrimitiveHelp(stringPluginName, primitiveType, stringPrimitiveName);
		if (textHelp == undefined || textHelp == "") {
			textHelp = getLocalizedText("error.modalHelp.noDescription");
		}

		//add primitive name
		var primitiveType = localCache.get("modalPrimitiveType");
		var modalHelpTitle = document.getElementById("modalHelpTitle");
		var textHelpTitle = document.createTextNode(primitiveType + "." + stringPrimitiveName);
		modalHelpTitle.appendChild(textHelpTitle);

		//add text to the modal body
		var textParentNode = document.getElementById("textHelp");
		var span = document.createElement("span");
		var textAsNode = document.createTextNode(textHelp);

		span.appendChild(textAsNode);
		textParentNode.appendChild(span);

	}
	else {
		var errorText = getLocalizedText("error.modalHelpPrimitive.noSelection");
		var textParentNode = document.getElementById("textHelp");
		var textAsNode = document.createTextNode(errorText);
		var span = document.createElement("span");
		span.appendChild(textAsNode);
		textParentNode.appendChild(span);
	}
}

function retrievePrimitiveHelp(pluginName, primitiveType, primitiveName) {
	var cache = localCache.get("pluginInfo");
	var pluginNumber = cache["plugins"].length;
	var primitiveHelp;
	for (var i = 0; i < pluginNumber; i++) {
		if (pluginName == cache["plugins"][i]["name"]) {
			if (cache["plugins"][i].hasOwnProperty(primitiveType)) {
				for (var j = 0; j < cache["plugins"][i][primitiveType].length; j++) {
					if (cache["plugins"][i][primitiveType][j]["name"] == primitiveName) {
						primitiveHelp = cache["plugins"][i][primitiveType][j]["help"];
					}
				}
			}
		}
	}
	return primitiveHelp;
}
/*------------------------------------------------------------------	
-------------------------------Plugin-------------------------------
-------------------------------------------------------------------*/
function displayHelpPlugin() {
	var helpWindow = document.getElementById("modalHelp");
	helpWindow.removeAttribute("hidden");

	clearModalHelp();

	var pluginName = document.getElementById("pluginNameSelect");
	if (pluginName.options.length > 0) {
		var stringPluginName = pluginName.options[pluginName.selectedIndex].text;
		var textHelp = retrievePluginHelp(stringPluginName);
		if (textHelp == undefined || textHelp == "") {
			textHelp = getLocalizedText("error.modalHelp.noDescription");
		}

		//add test and plugin name
		var modalHelpTitle = document.getElementById("modalHelpTitle");
		var textHelpTitle = document.createTextNode(stringPluginName);
		modalHelpTitle.appendChild(textHelpTitle);

		//add text to the modal body
		var textParentNode = document.getElementById("textHelp");
		var span = document.createElement("span");
		var textAsNode = document.createTextNode(textHelp);

		span.appendChild(textAsNode);
		textParentNode.appendChild(span);

	}
	else {
		var errorText = getLocalizedText("error.modalHelpPlugin.noSelection");
		var textParentNode = document.getElementById("textHelp");
		var textAsNode = document.createTextNode(errorText);
		var span = document.createElement("span");
		span.appendChild(textAsNode);
		textParentNode.appendChild(span);
	}
}

function retrievePluginHelp(pluginName) {

	var cache = localCache.get("pluginInfo");
	var pluginNumber = cache["plugins"].length;
	var pluginHelp;
	for (var i = 0; i < pluginNumber; i++) {
		if (pluginName == cache["plugins"][i]["name"]) {
			pluginHelp = cache["plugins"][i]["help"];
		}
	}
	return pluginHelp;
}