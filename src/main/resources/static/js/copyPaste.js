/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2021 Antoine Thevenet
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

/**This function retreive the text selected by the user on the ace editor
 *@returns {string} the selected text
 */
function getSelectionText(){
    var selectedText = "";
	var editor = ace.edit(getActiveEditor());
	selectedText = editor.session.getTextRange(editor.getSelectionRange());
    return selectedText;
}

/**This function retreive the text selected by the user on the ace editor.
 * It also deletes the selected text
 *@returns {string} the selected text
 */
function cutSelectionText(){
    var selectedText = "";
	var editor = ace.edit(getActiveEditor());
	var range = editor.selection.getRange();
	selectedText = editor.session.getTextRange(editor.getSelectionRange());
	editor.session.replace(range, "");
    return selectedText;
}

/**This function copy a selected text and add it to the clipboard. 
 * It uses the clipboard API
 */
function copyAceMenu() {
	var selected = getSelectionText();
	if (selected.length > 0){
		navigator.clipboard.writeText(selected).then(function(){
			/* success */
		}, function() {
			/* failure */
		});
	}
}

/**This function cut a selected text and add it to the clipboard. 
 * It uses the clipboard API
 */
function cutAceMenu() {
	var selected = cutSelectionText();
	if (selected.length > 0){
		navigator.clipboard.writeText(selected).then(function(){
			/* success */
		}, function() {
			/* failure */
		});
	}
}