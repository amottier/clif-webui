var assistant = require('../src/main/resources/static/js/assistant')
var assert = require('assert')

let string = "<scenario>\n<behaviors>\n<plugins>\n<use id=\"FileReader_0\" name=\"FileReader\">\n<params>\n<param name=\"filename\" type=\"String\"></param>\n<param name=\"charset\" type=\"String\"></param>\n</params>\n</use>\n<use id=\"Common_0\" name=\"Common\">\n<params></params>\n</use>\n<use id=\"Counter_0\" name=\"Counter\">\n<params>\n<param name=\"value_arg\" type=\"String\"></param>\n<param name=\"shared_arg\" type=\"String\"></param>\n<param name=\"radix_arg\" type=\"String\"></param>\n</params>\n</use>\n</plugins>\n<behavior id=\"B0\">\n<control use=\"FileReader_0\" name=\"clear\"></control>\n<control use=\"FileReader_0\" name=\"clear\"></control>\n<sample use=\"Common_0\" name=\"log\">\n<param name=\"duration\" type=\"String\"></param>\n<param name=\"comment\" type=\"String\"></param>\n<param name=\"result\" type=\"String\"></param>\n<param name=\"successful\" type=\"String\"></param>\n<param name=\"iteration\" type=\"String\"></param>\n</sample>\n</behavior>\n<behavior id=\"B1\"></behavior>\n<behavior id=\"B2\"></behavior>\n</behaviors>\n<loadprofile>\n</loadprofile>\n</scenario>\n";
let string2 = "<scenario><behaviors><plugins><use id=\"FileReader_0\" name=\"FileReader\">\n<params>\n<param name=\"filename\" type=\"String\"></param>\n<param name=\"charset\" type=\"String\"></param>\n</params>\n</use>\n<use id=\"Common_0\" name=\"Common\">\n<params></params>\n</use>\n<use id=\"Counter_0\" name=\"Counter\">\n<params>\n<param name=\"value_arg\" type=\"String\"></param>\n<param name=\"shared_arg\" type=\"String\"></param>\n<param name=\"radix_arg\" type=\"String\"></param>\n</params>\n</use>\n</plugins>\n<behavior id=\"B0\">\n<control use=\"FileReader_0\" name=\"clear\"></control>\n<control use=\"FileReader_0\" name=\"clear\"></control>\n<sample use=\"Common_0\" name=\"log\">\n<param name=\"duration\" type=\"String\"></param>\n<param name=\"comment\" type=\"String\"></param>\n<param name=\"result\" type=\"String\"></param>\n<param name=\"successful\" type=\"String\"></param>\n<param name=\"iteration\" type=\"String\"></param>\n</sample>\n</behavior>\n<behavior id=\"B1\"></behavior>\n<behavior id=\"B2\"></behavior>\n</behaviors>\n<loadprofile>\n</loadprofile>\n</scenario>\n";
let min = "<scenario>\n<behaviors>\n</behaviors>\n</scenario>"
let behaviorX1 ="<scenario>\n<behaviors>\n<behavior></behavior></behaviors>\n</scenario>"
let behaviorX2 ="<scenario><behaviors><behavior></behavior><behavior></behavior></behaviors></scenario>"
let testWithoutTab ="<aaa></aaa>\n"

describe('assistant', function() {

	describe('addTab', function() {
		it('should add one tab, min text', function() {
			assert.deepStrictEqual(assistant.addTabXml(testWithoutTab,[9, 0], min), ["\t<aaa></aaa>\n",""])
			
		})
		it('should add two tab, min text', function() {
			assert.deepStrictEqual(assistant.addTabXml(testWithoutTab,[21, 0], min), ["\t\t<aaa></aaa>\n",""])
			
		})
		it('should add two tabs, min text', function() {
			assert.deepStrictEqual(assistant.addTabXml(testWithoutTab,[16, 0], min), ["\t\t<aaa></aaa>\n",""])
			
		})
		
		it('should add two tabs, clif text', function() {
			assert.deepStrictEqual(assistant.addTabXml(testWithoutTab,[23, 0], string), ["\t\t<aaa></aaa>\n",""])
			
		})
		
		it('should add two tabs, clif text, one line', function() {
			assert.deepStrictEqual(assistant.addTabXml(testWithoutTab,[21, 0], string), ["\t\t<aaa></aaa>\n",""])
			
		})
		
		it('should add two tabs, clif text, one line, one behavior', function() {
			assert.deepStrictEqual(assistant.addTabXml(testWithoutTab,[44, 0], behaviorX1), ["\t\t<aaa></aaa>\n",""])
			
		})
		
		it('should add two tabs, clif text, one line, two behavior', function() {
			assert.deepStrictEqual(assistant.addTabXml(testWithoutTab,[63, 0], behaviorX2), ["\t\t<aaa></aaa>\n",""])
			
		})
		
		it('should add 0 tabs, clif text, one line, two behavior', function() {
			assert.deepStrictEqual(assistant.addTabXml(testWithoutTab,[85, 0], behaviorX2), ["\t\t<aaa></aaa>\n",""])
			
		})
		
	})

})
