::set basedir = "%~dp0"
::type basedir/isac-plugins.json

cat << 'END_OF_FILE'
{
   "plugins": [
       {
           "name": "CommandLineInjector",
           "help": "This plug-in supports execution of command lines, specific to your Operating System (e.g. cmd.exe for Windows, /bin/sh for Unix-like systems). Once a command has been executed, 3 variables are available to get its results: * stdout contains the command printed output * stderr contains the comman error output * retcode contains the integer return code of the command",
           "params": [],
           "samples": [
               {
                   "name": "execute",
                   "help": "Executes the provided command line. A comment may be provided at your convenience to provide helpful information when analyzing the test logs.",
                   "params": [
                       "command",
                       "comment",
                       "iteration"
                   ]
               }
           ]
       },
       {
           "name": "DnsInjector",
           "help": "This plugin sends DNS queries to the specified server,",
           "params": [
               "server_ipv4_arg",
               "server_port_arg",
               "delay_arg",
               "sig_algo_arg",
               "sig_key_name_arg",
               "sig_key_value_arg",
               "delail_report_arg",
               "ip_src_arg",
               "server_ipv6_arg",
               "dnssec_zone_file_arg",
               "dnssec_verify_sig_arg"
           ],
           "samples": [
               {
                   "name": "dns_query",
                   "help": "Send a DNS Query with arguments: - target : the domain name - type : in the list * A * AAAA * CNAME * DNAME * MX * NS * PTR * SOA * TXT * NULL - the expected answer (optional) - RD flag: 0/1 - D0 bit: 0/1 - CD flag: 0/1 - UDP: 0/1 If answer is OK, result can be retrieved by querying the variable ${DnsInjector_0:result}",
                   "params": [
                       "target_arg",
                       "query_type_arg",
                       "src_address_arg",
                       "expected_response_arg",
                       "recursion_desired_arg",
                       "dnssec_arg",
                       "checking_disable_arg",
                       "udp_arg"
                   ]
               }
           ],
           "tests": [
               {
                   "name": "DNS_isQueryAnswerOk",
                   "help": "Test if answer to DNS Query is OK",
                   "params": []
               }
           ]
       },
       {
           "name": "DnsProvider",
           "help": "The Domain Name target is built as follows: label<index><.zone> Index is incremented and possibly limited to a maximum value. The zone information is retrieved from a file. Each zone item is weighted. The different Query types are retrieved from a file. Each type item is weighted. The different IP source addresses are retrieved from a file. Each IP address item is weighted. A IPv6 in nibble reverse dotted format is generated and incremented to test reverse DNS in IPv6. A IPv4 in reverse dotted format is generated and incremented to test reverse DNS in IPv4. Retrieve data by entrering values: ${DnsProvider_0:domain} for getting domain name ${DnsProvider_0:query} for getting query type ${DnsProvider_0:ip} for getting source IP ${DnsProvider_0:dnssec} for getting DNSSEC ${DnsProvider_0:rd} for getting RD flag (0/1) ${DnsProvider_0:udp} for getting UDP protocol (vs. TCP) (0/1) ${DnsProvider_0:ip6rev} : to get the IPv6 @ in nibble reverse dotted format. ${DnsProvider_0:ip6for} : to get the IPv6 @ in nibble forward format. ${DnsProvider_0:ip4rev} : to get the IPv4 @ in reverse dotted format.",
           "params": [windows dirname $0
               "format_arg",
               "first_index_domain_arg",
               "last_index_domain_arg",
               "qtype_arg",
               "ip_src_arg",
               "index_length_arg",
               "zone_arg",
               "rd_flag_arg",
               "udp_arg",
               "dnssec_arg",
               "IP6reverse",
               "IP4reverse"
           ],
           "controls": [
               {
                   "name": "next",
                   "help": "Compute next data. Data can be retrieved by invoking: ${DnsProvider_0:domain} : to get the target domain name. ${DnsProvider_0:query} : to get the query type. ${DnsProvider_0:ip} : to get the IP source address. ${DnsProvider_0:rd} : to get the Recursion Desired Flag (0/1). ${DnsProvider_0:udp} : to get the UDP (vs. TCP) flag (0/1). ${DnsProvider_0:dnssec} : to get the DNSSEC flag (0/1). ${DnsProvider_0:ip6rev} : to get the IPv6 @ in nibble reverse dotted format. ${DnsProvider_0:ip6for} : to get the IPv6 @ in nibble forward format. ${DnsProvider_0:ip4rev} : to get the IPv4 @ in reverse dotted format.",
                   "params": []
               }
           ]
       },
       {
           "name": "FtpInjector",
           "help": "Load injector for FTP servers. Use connect first before performing any other command. Provided variables: ${plugin-id:reply code} gives the reply code of the last command, ${plugin-id:replay string} gives the full reply message.",
           "params": [],
           "controls": [
               {
                   "name": "lcd",
                   "help": "Changes local current directory.",
                   "params": [
                       "path"
                   ]
               }
           ],
           "samples": [
               {
                   "name": "cd",
                   "help": "Changes current remote directory. Must be connected.",
                   "params": [
                       "path"
                   ]
               },
               {
                   "name": "connect",
                   "help": "Connects to an FTP server.",
                   "params": [
                       "host",
                       "port",
                       "localaddress",
                       "comment"
                   ]
               },
               {
                   "name": "disconnect",
                   "help": "Disconnects from the FTP server. Must be connected already.",
                   "params": [
                       "comment"
                   ]
               },
               {
                   "name": "login",
                   "help": "Log in the server, giving authentication information. Must be connected already.",
                   "params": [
                       "login",
                       "password",
                       "comment"
                   ]
               },
               {
                   "name": "logout",
                   "help": "Log out from the server. Must be connected already.",
                   "params": [
                       "comment"
                   ]
               },
               {
                   "name": "noop",
                   "help": "Send noop command (convenience to avoid time-out driven disconnections by the server). Must be connected already.",
                   "params": [
                       "comment"
                   ]
               },
               {
                   "name": "retrieve",
                   "help": "Download a file from the server. Must be connected already.",
                   "params": [
                       "remote file",
                       "destination",
                       "local file",
                       "policy",
                       "size",
                       "time"
                   ]
               },
               {
                   "name": "setFileType",
                   "help": "Set data type for next file transfers. Must be connected already.",
                   "params": [
                       "type",
                       "byte size",
                       "format"
                   ]
               },
               {
                   "name": "store",
                   "help": "Upload a file to the FTP server. Must be connected already.",
                   "params": [
                       "content",
                       "file",
                       "limit",
                       "size",
                       "time",
                       "remote file"
                   ]
               },
               {
                   "name": "delete",
                   "help": "Deletes a file on the server. Must be connected already.",
                   "params": [
                       "file"
                   ]
               }
           ],
           "tests": [
               {
                   "name": "connected",
                   "help": "True if the FTP client is currently connected to a server, false otherwise, or the contrary if the \"not\" option is enabled.",
                   "params": [
                       "options"
                   ]
               },
               {
                   "name": "loggedIn",
                   "help": "True if the FTP client is currently logged in, false otherwise, or the contrary if the \"not\" option is enabled.",
                   "params": [
                       "options"
                   ]
               },
               {
                   "name": "replyCodeIs",
                   "help": "Tests the reply code value regarding the last FTP command issued.",
                   "params": [
                       "code",
                       "options"
                   ]
               }
           ]
       },
       {
           "name": "GitInjector",
           "help": "Give object help.",
           "params": [],
           "samples": [
               {
                   "name": "fetch",
                   "help": "Fetch operation (retrieves content from the remote repository to the local repository). The default action type is \"GIT FETCH\" and the default comment is the reference advertized by the remote GIT repository. Both the action type and the comment may be overridden with custom values.",
                   "params": [
                       "remote",
                       "login",
                       "password",
                       "refspec",
                       "comment",
                       "type"
                   ]
               }
           ]
       },
       {
           "name": "HttpInjector",
           "help": "Support for HTTP protocol: get, post, multipart post, head, options, put, delete. By default, each of these methods just gets the given URL without analyzing its content. Optionally, the content may be analyzed to transparently include referenced external resources, such as frames, images, style sheets and scripts Provides conditions on response status code and headers. Provides a number of variables: user-defined variables to get response bodies, variables to get headers values ${HttpInj:#header_name}, and a variable to get the response status code ${HttpInj:!}.",
           "params": [
               "indepthload",
               "proxyhost",
               "proxyport",
               "proxyusername",
               "proxyuserpass",
               "preemptiveauthentication",
               "useragent",
               "unit",
               "cookieheader"
           ],
           "controls": [
               {
                   "name": "setConnectionTimeOut",
                   "help": "Sets the maximum delay (in milliseconds) waiting for the target server to accept the connection. When this delay is reached, the request is considered as a failure. A zero time-out value disables the time-out feature (infinite time-out).",
                   "params": [
                       "timeout"
                   ]
               },
               {
                   "name": "setResponseTimeOut",
                   "help": "Sets the maximum delay (in milliseconds) waiting for a request response. When this delay is reached, the request is considered as a failure. A zero time-out value disables the time-out feature (infinite time-out).",
                   "params": [
                       "timeout"
                   ]
               },
               {
                   "name": "setErrorResponseCodes",
                   "help": "The HttpInjector states that an HTTP request is successful as long as it gets a response from the server, whatever the response code is. Here, you may specify that some chosen response codes must be considered as errors: you may provide either full 3-digits HTTP response codes, or just one or two digits prefixes. When a response code begins with one of the provided prefix, or equals one of the provided codes, the request is considered as failed.",
                   "params": [
                       "codes"
                   ]
               }
           ],
           "samples": [
               {
                   "name": "get",
                   "params": [
                       "uri",
                       "redirect",
                       "type",
                       "comment",
                       "headers",
                       "parameters",
                       "response-body",
                       "username",
                       "password",
                       "hostauth",
                       "realm",
                       "proxylogin",
                       "proxypassword",
                       "cookies",
                       "cookiepolicy",
                       "proxyhost",
                       "proxyport",
                       "localaddress"
                   ]
               },
               {
                   "name": "post",
                   "help": "The POST method is used to request that the origin server accept the data enclosed in the request as a new child of the request URL. POST is designed to allow a uniform method to cover a variety of functions such as appending to a database, providing data to a data-handling process or posting to a message board. Parameters for the POST method : ------------------------------- uri: an Uri beginning with http://. It defines the target URL redirect: set to enable automatic HTTP redirections (3XX code) parameters: set the parameters to be included in the URI string body alternatives: either a file, or a set of key-value pairs (typically for HTML forms), or a string. The specified Content-Type header is used for content type and character encoding. response-body: the response body may be optionally stored in a variable with the given name. This variable's value can be retrieved with the usual expression ${plug-inId:variableName}. Configuration of the authentication : --------------------------------------- username/password : set the identifiers realm : the authentication realm host : the host the realm belongs to Configuration of the Cookies : ------------------------------ cookies : a table of cookie definitions : name: the cookie name value: the cookie value domain: the domain this cookie can be sent to path: the path prefix for which this cookie can be sent maxAge : the number of seconds for which this cookie is valid. maxAge is expected to be a non-negative number. (-1 signifies that the cookie should never expire) expires : a date in the format 'YY/MM/DD/hh/mm/ss' secure: if true this cookie can only be sent over secure (define expires or maxAge or neither) cookie policy : define the policy to follow (by default 'compatibility') Configuration of the Proxy : ---------------------------- proxyhost : host of the proxy proxyport : port of the proxy proxylogin : login to be used if the proxy needs authentication proxypassword : password corresponding localadress : the virtual host name",
                   "params": [
                       "uri",
                       "redirect",
                       "type",
                       "comment",
                       "headers",
                       "parameters",
                       "bodyparameters",
                       "request",
                       "file",
                       "response-body",
                       "username",
                       "password",
                       "hostauth",
                       "realm",
                       "proxylogin",
                       "proxypassword",
                       "cookies",
                       "cookiepolicy",
                       "proxyhost",
                       "proxyport",
                       "localaddress"
                   ]
               },
               {
                   "name": "multipartpost",
                   "help": "The multipart post method is identical to the POST method, except that the request body is separated into multiple parts. This method is generally used when uploading files to the server. Parameters for the MULTIPARTPOST method : ----------------------------------------- uri : an Uri beginning with http://. It define the URL and gets the document the URL points to redirect : set to enabled in order to follow redirection (3XX code) parameters : set the parameters that were send with the request files : a table of the file to multipost Configuration of the authentication : --------------------------------------- username/password : set the identifiers realm : the authentication realm host : the host the realm belongs to Configuration of the Cookies : ------------------------------ cookies : a table of cookie definitions : name: the cookie name value: the cookie value domain: the domain this cookie can be sent to path: the path prefix for which this cookie can be sent maxAge : the number of seconds for which this cookie is valid. maxAge is expected to be a non-negative number. (-1 signifies that the cookie should never expire) expires : a date in the format 'YY/MM/DD/hh/mm/ss' secure: if true this cookie can only be sent over secure (define expires or maxAge or neither) cookie policy : define the policy to follow (by default 'compatibility') Configuration of the Proxy : ---------------------------- proxyhost : host of the proxy proxyport : port of the proxy proxylogin : login to be used if the proxy needs authentication proxypassword : password corresponding localadress : the virtual host name",
                   "params": [
                       "uri",
                       "redirect",
                       "type",
                       "comment",
                       "headers",
                       "parameters",
                       "files",
                       "response-body",
                       "username",
                       "password",
                       "hostauth",
                       "realm",
                       "proxylogin",
                       "proxypassword",
                       "cookies",
                       "cookiepolicy",
                       "proxyhost",
                       "proxyport",
                       "localaddress"
                   ]
               },
               {
                   "name": "head",
                   "help": "The HEAD method is identical to GET except that the server must not return a message-body in the response. This method can be used for obtaining metainformation about the document implied by the request without transferring the document itself. Parameters for the HEAD method : ------------------------------- uri : an Uri beginning with 'http://'. redirect : set to enabled in order to follow redirection (3XX code) parameters : set the parameters that were send with the request Configuration of the authentication : --------------------------------------- username/password : set the identifiers realm : the authentication realm host : the host the realm belongs to Configuration of the Cookies : ------------------------------ cookies : a table of cookie definitions : name: the cookie name value: the cookie value domain: the domain this cookie can be sent to path: the path prefix for which this cookie can be sent maxAge : the number of seconds for which this cookie is valid. maxAge is expected to be a non-negative number. (-1 signifies that the cookie should never expire) expires : a date in the format 'YY/MM/DD/hh/mm/ss' secure: if true this cookie can only be sent over secure (define expires or maxAge or neither) cookie policy : define the policy to follow (by default 'compatibility') Configuration of the Proxy : ---------------------------- proxyhost : host of the proxy proxyport : port of the proxy proxylogin : login to be used if the proxy needs authentication proxypassword : password corresponding localadress : the virtual host name",
                   "params": [
                       "uri",
                       "redirect",
                       "type",
                       "comment",
                       "headers",
                       "parameters",
                       "username",
                       "password",
                       "hostauth",
                       "realm",
                       "proxylogin",
                       "proxypassword",
                       "cookies",
                       "cookiepolicy",
                       "proxyhost",
                       "proxyport",
                       "localaddress"
                   ]
               },
               {
                   "name": "options",
                   "help": "The OPTIONS method represents a request for information about the communication options available on the request/response chain identified by the request URL. Parameters for the OPTIONS method : ------------------------------- uri : an Uri beginning with 'http://'. redirect : set to enabled in order to follow redirection (3XX code) parameters : set the parameters that were send with the request Configuration of the authentication : --------------------------------------- username/password : set the identifiers realm : the authentication realm host : the host the realm belongs to Configuration of the Cookies : ------------------------------ cookies : a table of cookie definitions : name: the cookie name value: the cookie value domain: the domain this cookie can be sent to path: the path prefix for which this cookie can be sent maxAge : the number of seconds for which this cookie is valid. maxAge is expected to be a non-negative number. (-1 signifies that the cookie should never expire) expires : a date in the format 'YY/MM/DD/hh/mm/ss' secure: if true this cookie can only be sent over secure (define expires or maxAge or neither) cookie policy : define the policy to follow (by default 'compatibility') Configuration of the Proxy : ---------------------------- proxyhost : host of the proxy proxyport : port of the proxy proxylogin : login to be used if the proxy needs authentication proxypassword : password corresponding localadress : the virtual host name",
                   "params": [
                       "uri",
                       "redirect",
                       "type",
                       "comment",
                       "headers",
                       "parameters",
                       "response-body",
                       "username",
                       "password",
                       "hostauth",
                       "realm",
                       "proxylogin",
                       "proxypassword",
                       "cookies",
                       "cookiepolicy",
                       "proxyhost",
                       "proxyport",
                       "localaddress"
                   ]
               },
               {
                   "name": "put",
                   "help": "The PUT method requests that the enclosed document be stored under the supplied URL. This method is generally disabled on publicly available servers because it is generally undesirable to allow clients to put new files on the server or to replace existing files. Parameters for the PUT method : ------------------------------- uri : an Uri beginning with 'http://'. redirect : set to enabled in order to follow redirection (3XX code) parameters : set the parameters that were send with the request file : the name of the file to put filetype : the type of the file to put. (optional) Configuration of the authentication : --------------------------------------- username/password : set the identifiers realm : the authentication realm host : the host the realm belongs to Configuration of the Cookies : ------------------------------ cookies : a table of cookie definitions : name: the cookie name value: the cookie value domain: the domain this cookie can be sent to path: the path prefix for which this cookie can be sent maxAge : the number of seconds for which this cookie is valid. maxAge is expected to be a non-negative number. (-1 signifies that the cookie should never expire) expires : a date in the format 'YY/MM/DD/hh/mm/ss' secure: if true this cookie can only be sent over secure (define expires or maxAge or neither) cookie policy : define the policy to follow (by default 'compatibility') Configuration of the Proxy : ---------------------------- proxyhost : host of the proxy proxyport : port of the proxy proxylogin : login to be used if the proxy needs authentication proxypassword : password corresponding localadress : the virtual host name",
                   "params": [
                       "uri",
                       "redirect",
                       "type",
                       "comment",
                       "headers",
                       "parameters",
                       "request",
                       "response-body",
                       "file",
                       "filetype",
                       "username",
                       "password",
                       "hostauth",
                       "realm",
                       "proxylogin",
                       "proxypassword",
                       "cookies",
                       "cookiepolicy",
                       "proxyhost",
                       "proxyport",
                       "localaddress"
                   ]
               },
               {
                   "name": "delete",
                   "help": "The DELETE method requests that the server delete the resource identified by the request URL. This method is generally disabled on publicly available servers because it is generally undesireable to allow clients to delete files on the server. Parameters for the DELETE method : ------------------------------- uri : an Uri beginning with 'http://'. redirect : set to enabled in order to follow redirection (3XX code) parameters : set the parameters that were send with the request Configuration of the authentication : --------------------------------------- username/password : set the identifiers realm : the authentication realm host : the host the realm belongs to Configuration of the Cookies : ------------------------------ cookies : a table of cookie definitions : name: the cookie name value: the cookie value domain: the domain this cookie can be sent to path: the path prefix for which this cookie can be sent maxAge : the number of seconds for which this cookie is valid. maxAge is expected to be a non-negative number. (-1 signifies that the cookie should never expire) expires : a date in the format 'YY/MM/DD/hh/mm/ss' secure: if true this cookie can only be sent over secure (define expires or maxAge or neither) cookie policy : define the policy to follow (by default 'compatibility') Configuration of the Proxy : ---------------------------- proxyhost : host of the proxy proxyport : port of the proxy proxylogin : login to be used if the proxy needs authentication proxypassword : password corresponding localadress : the virtual host name",
                   "params": [
                       "uri",
                       "redirect",
                       "type",
                       "headers",
                       "comment",
                       "parameters",
                       "response-body",
                       "username",
                       "password",
                       "hostauth",
                       "realm",
                       "proxylogin",
                       "proxypassword",
                       "cookies",
                       "cookiepolicy",
                       "proxyhost",
                       "proxyport",
                       "localaddress"
                   ]
               }
           ],
           "tests": [
               {
                   "name": "is404Response",
                   "help": "True if the last response status was 404, false otherwise.",
                   "params": []
               },
               {
                   "name": "isStatusCodeResponse",
                   "help": "True if the last response status code equals, or starts with, the given value or prefix; false otherwise.",
                   "params": [
                       "statuscode"
                   ]
               },
               {
                   "name": "isHeaderValue",
                   "help": "Checks a header value in the latest response.",
                   "params": [
                       "headervalue",
                       "headertype",
                       "equality"
                   ]
               },
               {
                   "name": "hasHeader",
                   "help": "True if the given header is defined in the latest response, false otherwise.",
                   "params": [
                       "headertype"
                   ]
               },
               {
                   "name": "hasNoHeader",
                   "help": "True if the given header is undefined in the latest response, false otherwise.",
                   "params": [
                       "headertype"
                   ]
               },
               {
                   "name": "statusIsOneOf",
                   "help": "True if the last response status code equals one of the given values; false otherwise.",
                   "params": [
                       "statuscodes"
                   ]
               },
               {
                   "name": "statusIsNotOneOf",
                   "help": "True if the last response status code differs from all the given values; false otherwise.",
                   "params": [
                       "statuscodes"
                   ]
               }
           ]
       },
       {
           "name": "ImapInjector",
           "help": "This plugin is used for testing an IMAP mail server. It provides methods to connect, login, select mailboxes(folders), search messages, retrieve messages, copy messages from one mailbox(folder) to another, and logout.",
           "params": [
               "hostname",
               "port"
           ],
           "samples": [
               {
                   "name": "Connect",
                   "help": "This connects to the hostname and port configured",
                   "params": [
                       "username",
                       "password",
                       "hostname",
                       "port"
                   ]
               },
               {
                   "name": "logoff",
                   "help": "This logs the current user off, and terminates the connection.",
                   "params": []
               },
               {
                   "name": "select",
                   "help": "This selects a given mailbox/folder. When selecting a sub-folder, the full path must be specified. Example: INBOX\\junk",
                   "params": [
                       "name"
                   ]
               },
               {
                   "name": "search",
                   "help": "This executes a search on the server. Search Criteria can be a complex logical string. Please refer to the IMAP RFC (#3501) for details on valid keywords and operators.",
                   "params": [
                       "criteria"
                   ]
               },
               {
                   "name": "fetch",
                   "help": "This retrieves a specified message. A mailbox must be selected before making this request. Message ID can be an individual message number, or a range, in the form x:y. The range is inclusive.",
                   "params": [
                       "messageID"
                   ]
               },
               {
                   "name": "move",
                   "help": "This movies the designated message(s) to a new location/mailbox. The message(s) will be deleted from the source location. Message ID can be an individual message number, or a range, in the form x:y. The range is inclusive. Mailbox/Folder Name specifies where to copy the message(s) to. If the destination is a sub-folder, the full path must be specified. Example: INBOX\\junk",
                   "params": [
                       "messageID",
                       "mailbox"
                   ]
               }
           ]
       },
       {
           "name": "JdbcInjector",
           "help": "Provides a JDBC interface to query a RDBMS. Caution: unlike common ISAC plug-ins, all response times are measured as MICROSECONDS!",
           "params": [
               "serverType",
               "host",
               "port",
               "login",
               "passwd",
               "base_name"
           ],
           "controls": [
               {
                   "name": "set_autocommit",
                   "help": "Switch on/off transaction autocommit for the connection.",
                   "params": [
                       "autocommit"
                   ]
               },
               {
                   "name": "set_isolation_level",
                   "help": "Set transaction isolation level.",
                   "params": [
                       "isolationLevel"
                   ]
               }
           ],
           "samples": [
               {
                   "name": "executeQuery",
                   "help": "This sample simply executes a given query. It invoques executeQuery( ... ) method on a Statement object related to your connection. Caution: response time is measured in *microseconds*.",
                   "params": [
                       "sqlStmt",
                       "useResultSet",
                       "resultSetTypes"
                   ]
               },
               {
                   "name": "executeUpdate",
                   "help": "This sample simply executes a given query. It invoques executeUpdate( ... ) on a Statement object related to your connection. Caution: response time is measured in *microseconds*.",
                   "params": [
                       "sqlStmt"
                   ]
               },
               {
                   "name": "prepareStatement",
                   "help": "Use this sample if you want to \"prepare\" a statement. parameters: - Name : as you can prepare multiple statements for one connection, you have to give it a name. This name is required to execute the statement. - Statement : specifie the SQL query you want to send using placeholder markers. - Place holders : enumerate the parameter types (refer to the JDBC specification) (! only INTEGER and VARCHAR has been tested for the moment ! the others will come soon) Caution: response time is measured in *microseconds*.",
                   "params": [
                       "preparedStmtName",
                       "sqlStmt",
                       "placeHolders",
                       "useResultSet",
                       "resultSetTypes"
                   ]
               },
               {
                   "name": "execute",
                   "help": "Use this sample to execute your prepared statement. parameters: - Name : is the name of a prepared statement previously declared - Object : not used yet - Method : specify the method you want to use (refer to JDBC specification) - Values : gives values to place holders Caution: response time is measured in *microseconds*.",
                   "params": [
                       "preparedStmtName",
                       "object",
                       "executeMethod",
                       "placeHoldersValues"
                   ]
               },
               {
                   "name": "connect",
                   "help": "not used yet",
                   "params": []
               },
               {
                   "name": "commit",
                   "help": "Commit current transaction. Caution: response time is measured in *microseconds*.",
                   "params": []
               },
               {
                   "name": "rollback",
                   "help": "Rollback current transaction. Caution: response time is measured in *microseconds*.",
                   "params": []
               }
           ]
       },
       {
           "name": "JmsInjector",
           "help": "This plugin allows to inject charge in Joram cluster queues. It provides some methods which allow to start consumer and producer clients",
           "params": [
               "clusterQueueName",
               "connectionFactoryName",
               "messageSize",
               "jndiHost",
               "jndiPort"
           ],
           "samples": [
               {
                   "name": "startConsumer",
                   "params": [
                       "nbMsg",
                       "consWeight"
                   ]
               },
               {
                   "name": "startProducer",
                   "params": [
                       "nbMsg",
                       "prodWeight"
                   ]
               }
           ]
       },
       {
           "name": "LdapInjector",
           "help": "This plugin allows to test an LDAP V3 directory. It provides primitives to connect (with or without SSL), search, add or delete entries, and add or delete attributes.",
           "params": [
               "ldapHost",
               "ldapVersion",
               "ldapPort",
               "ldapSSLPort"
           ],
           "samples": [
               {
                   "name": "bind",
                   "help": "Connects and binds to an LDAP directory. This sample must be performed at least once prior to any other sample. Leave login and password empty for an anonymous bind. When trying to connect over SSL (LDAPS), beware of setting Java properties regarding trusted certificates: the LDAP server certificate, when not signed by a recognized certification authority, must be added to a Java trust store file, possibly protected by a password. Both the path to the trust store file and the associated password must be specified via these Java system properties: -Djavax.net.ssl.trustStore=/path/to/keystore -Djavax.net.ssl.trustStorePassword=the_keystore_password",
                   "params": [
                       "login",
                       "password",
                       "withSSL",
                       "actiontype",
                       "comment"
                   ]
               },
               {
                   "name": "search",
                   "help": "Performs a search operation in the LDAP directory. A successful call to sample 'bind' is mandatory prior to running any search. The number of result entries may be limited unless this limit is set to 0, which means unlimited. Search results may be stored in a buffer for further reference via a number of variable statements: - ${myLdap:bufferName:#} number of entries left to read; - ${myLdap:bufferName:} gets the next entry and returns all of its attributes as name-value pairs; - ${myLdap:bufferName:attributeName} gets the next entry and returns the value of its attribute whose name is given.",
                   "params": [
                       "searchBase",
                       "searchFilter",
                       "searchScope",
                       "actiontype",
                       "comment",
                       "buffer",
                       "searchLimit"
                   ]
               },
               {
                   "name": "closeConnection",
                   "help": "Disconnects from the LDAP server. A successful call to sample 'bind' is mandatory prior to calling 'closeConnection'.",
                   "params": []
               },
               {
                   "name": "addEntry",
                   "help": "Add entry into an ldap directory. A successful call to sample 'bind' is mandatory prior to adding an entry.",
                   "params": [
                       "ldapDN",
                       "entryNodeType",
                       "entryNodeName",
                       "AttributesList"
                   ]
               },
               {
                   "name": "deleteEntry",
                   "help": "Delete entry into an ldap directory. A successful call to sample 'bind' is mandatory prior to deleting an entry.",
                   "params": [
                       "ldapDN"
                   ]
               },
               {
                   "name": "addAttribute",
                   "help": "Add attributes into an entry of a ldap directory. A successful call to sample 'bind' is mandatory prior to adding attributes.",
                   "params": [
                       "dn",
                       "attributesNameValues"
                   ]
               },
               {
                   "name": "deleteAttribute",
                   "help": "Delete attributes of entries of a ldap directory. A successful call to sample 'bind' is mandatory prior to deleting attributes.",
                   "params": [
                       "dn",
                       "attributesToDelete"
                   ]
               }
           ],
           "tests": [
               {
                   "name": "isConnected",
                   "help": "True if the LDAP connection is established, false otherwise.",
                   "params": []
               },
               {
                   "name": "isNotConnected",
                   "help": "True if the LDAP connection is not established, false otherwise.",
                   "params": []
               },
               {
                   "name": "hasMoreEntries",
                   "help": "True if there is at least one entry left to read in a search results buffer, false otherwise.",
                   "params": [
                       "buffer"
                   ]
               },
               {
                   "name": "hasNoMoreEntries",
                   "help": "True if there is no more entry left to read in a search results buffer, false otherwise.",
                   "params": [
                       "buffer"
                   ]
               }
           ]
       },
       {
           "name": "MqttInjector",
           "help": "This ISAC plug-in supports connection and interaction with an MQTT server, as of version 3.1.1 of the MQTT specification. Once connected, the virtual user may publish messages on topics and subscribe to topics. The payload of messages received from a topic subscription may be retrieved and made available as plug-in variables (see sample getMessage, conditions isDefined/isNotDefined and control clear to manage these variables).",
           "params": [
               "client id size",
               "library"
           ],
           "controls": [
               {
                   "name": "clear",
                   "help": "Clears a message previously retrieved and named by a call to sample getMessage. After control clear has been called on a given name, condition isDefined is false and condition isNotDefined is false for this name. Any further reference to the plug-in variable of this name would throw an exception.",
                   "params": [
                       "variable"
                   ]
               }
           ],
           "samples": [
               {
                   "name": "connect",
                   "help": "Connects to an MQTT server. Leave the client id input blank if you want it to be generated. Leave user name and password empty if authentication is not required. Any attempt to connect an already connected virtual user will fail and generate an exception. A connection time-out may be set in seconds, zero meaning no time-out (infinite connection delay). Once connected, the client identifier is available via variable #.",
                   "params": [
                       "host",
                       "port",
                       "protocol",
                       "action type",
                       "comment",
                       "clientid",
                       "username",
                       "password",
                       "timeout_s",
                       "message",
                       "qos",
                       "topic",
                       "flags",
                       "enable",
                       "keepalive_s"
                   ]
               },
               {
                   "name": "publish",
                   "help": "Publishes a message over a topic. This virtual user must be connected piori to publish a message.",
                   "params": [
                       "message",
                       "qos",
                       "topic",
                       "action type",
                       "comment",
                       "flags"
                   ]
               },
               {
                   "name": "disconnect",
                   "help": "Disconnects current virtual user from an MQTT server. Any attempt to disconnect a unconnected server will fail.",
                   "params": [
                       "action type",
                       "comment"
                   ]
               },
               {
                   "name": "subscribe",
                   "help": "Subscribes current virtual user to the given topic. The virtual user must be connected prior to subscribe. Once the virtual user has subscribed to a topic, received messages may be retrieved using sample getmessage. In order to enable this feature, the capacity of the incoming messages queue (in terms of maximum number of queued messages) must be set to a value greater than zero. A drop policy must also be chosen to decide what to do when the queue is full: either drop the oldest message, or drop the incoming message.",
                   "params": [
                       "topic",
                       "qos",
                       "action type",
                       "comment",
                       "size",
                       "policy"
                   ]
               },
               {
                   "name": "unsubscribe",
                   "help": "Unsubscribes current virtual user from a topic. The virtual user must be connected to an MQTT and have subscribed to the topic prior to unsubscribe.",
                   "params": [
                       "topic",
                       "action type",
                       "comment"
                   ]
               },
               {
                   "name": "getMessage",
                   "help": "Tries to retrieve a message from the message queue associated with one, several or all topic subscriptions (leave empty the list of topics for all currently subscribed topics). A negative timeout means infinite time-out. A zero timeout means no waiting time. If a message could be retrieved and a message name was provided, the message payload is then available as data from a plug-in variable with this name: ${myMqtt:message var}. Moreover, conditions isDefined and isNotDefined are set accordingly for this variable name, making it possible to check whether a message could be retrieved before trying to get the corresponding variable value from the plug-in. Similarly, the actual topic the message was sent to can be retrieved through a variable.",
                   "params": [
                       "timeout",
                       "topics",
                       "action type",
                       "comment",
                       "topic var",
                       "message var",
                       "options"
                   ]
               }
           ],
           "tests": [
               {
                   "name": "isConnected",
                   "help": "True if this virtual user is connected to an MQTT server, false otherwise.",
                   "params": []
               },
               {
                   "name": "isNotConnected",
                   "help": "True if this virtual user is not connected to an MQTT server, false otherwise.",
                   "params": []
               },
               {
                   "name": "isDefined",
                   "help": "True if an incoming message (coming from a topic subscription) or its actual topic has been retrieved by sample getMessage and associated to the provided name. False if the last call to getMessage failed or if no name was associated with the retrieved message or associated topic, or if no call to getMessage has been performed since the topic subscription.",
                   "params": [
                       "variable"
                   ]
               },
               {
                   "name": "isNotDefined",
                   "help": "True if the provided name is unknown in this virtual user's incoming message queue or associated topic. False if the virtual user has subscribed to a topic, received a message on this topic and stored either the message payload or its actual topic in a variable with the same name, using sample getMessage.",
                   "params": [
                       "variable"
                   ]
               }
           ]
       },
       {
           "name": "RtpInjector",
           "help": "RtpInjector is a plug-in for CLIF. It allows you to send and receive RTP and RTCP packets to test an equipment or a software. This plug-in doesn't control yours parameters, so it is important to know how RTP works before using it. To add multiples vUsers, you have to use Counter plug-in or CsvProvider plug-in because it can have only one user per port. Fields with (*) are mandatory. Fields with (-) are automatically generated if they are empty, because they are mandatory. In this version, it permits to send request based on RFC 3550 and RFC 4733. Version: 1.0",
           "params": [],
           "controls": [
               {
                   "name": "initRtp",
                   "help": "Initialize a RTP Session.",
                   "params": [
                       "localIpAddress",
                       "localPort",
                       "remoteAddress",
                       "remotePort",
                       "timeOut",
                       "duration",
                       "Session ID"
                   ]
               },
               {
                   "name": "send",
                   "help": "Send a file.",
                   "params": [
                       "content",
                       "time",
                       "version",
                       "padding",
                       "extension",
                       "csrcCount",
                       "marker",
                       "payloadType",
                       "Session ID"
                   ]
               },
               {
                   "name": "forward",
                   "help": "Forward a RTP stream.",
                   "params": [
                       "listeningPort",
                       "duration",
                       "packetSize",
                       "Session ID"
                   ]
               },
               {
                   "name": "dtmf",
                   "help": "Send a DTMF digit using RFC 4733 (previously RFC 2833). DTMF using frequency are not implemented for now.",
                   "params": [
                       "digit",
                       "time",
                       "volume",
                       "dtmfType",
                       "version",
                       "padding",
                       "extension",
                       "csrcCount",
                       "marker",
                       "payloadType",
                       "Session ID"
                   ]
               },
               {
                   "name": "enableRtcp",
                   "help": "Enable RTCP in the RTP stream. It create a template of RTCP packet by adding followings controls : - addReport, - addSdes, - addApp. If you add control addBye, the RTCP BYE packet will be send at the end of the transmission.",
                   "params": [
                       "timeInterval",
                       "Session ID"
                   ]
               },
               {
                   "name": "addReport",
                   "help": "Add report to the template.",
                   "params": [
                       "Session ID"
                   ]
               },
               {
                   "name": "addSdes",
                   "help": "Add SDES to the template. Available items are : - CNAME, - NAME, - EMAIL, - PHONE, - LOC, - TOOL, - NOTE, - PRIV.",
                   "params": [
                       "items",
                       "Session ID"
                   ]
               },
               {
                   "name": "addBye",
                   "help": "Will send RTCP BYE at the end of the transmission.",
                   "params": [
                       "reason",
                       "Session ID"
                   ]
               },
               {
                   "name": "addApp",
                   "help": "Add APP to the template. The subtype must be a number includes between 0 and 31. The data to transmit in the packet must be less or egals to 500 bytes. Need FileReader plug-in to work. Add ${name_of_your_filereader_plugin:} to the field \"URL of the data file\".",
                   "params": [
                       "name",
                       "data",
                       "subtype",
                       "Session ID"
                   ]
               },
               {
                   "name": "resetRtcpPacket",
                   "help": "Reset the template of the RTCP packet and disable RTCP from the scenario.",
                   "params": [
                       "Session ID"
                   ]
               },
               {
                   "name": "sendBye",
                   "help": "Send a RTCP BYE for each users.",
                   "params": [
                       "reason",
                       "Session ID"
                   ]
               },
               {
                   "name": "sendReceiverReport",
                   "help": "Send RTCP receiver report.",
                   "params": [
                       "Session ID"
                   ]
               }
           ],
           "tests": [
               {
                   "name": "isRtcpBye",
                   "help": "Test if a BYE from this session have been received.",
                   "params": [
                       "notChoice",
                       "Session ID"
                   ]
               }
           ]
       },
       {
           "name": "SocketInjector",
           "help": "Manages a socket connection, for both reading and writing. Data read from the socket may be retrieved through variables whose names are chosen for each issued read.",
           "params": [
               "unit"
           ],
           "controls": [
               {
                   "name": "accept",
                   "help": "Creates a server socket and waits for client connection.",
                   "params": [
                       "localip",
                       "localport",
                       "timeout"
                   ]
               },
               {
                   "name": "setTimeOut",
                   "help": "Sets the timeout value for socket read operations. The socket must be open before setting a timeout (using 'accept' control or 'connect' sample).",
                   "params": [
                       "timeout"
                   ]
               }
           ],
           "samples": [
               {
                   "name": "close",
                   "help": "Closes the socket.",
                   "params": []
               },
               {
                   "name": "readBytes",
                   "help": "Reads bytes from the socket.",
                   "params": [
                       "buffername"
                   ]
               },
               {
                   "name": "readUTFString",
                   "help": "Reads an UTF string from the socket.",
                   "params": [
                       "buffername"
                   ]
               },
               {
                   "name": "writeBytes",
                   "help": "Writes bytes into the socket.",
                   "params": [
                       "datastring"
                   ]
               },
               {
                   "name": "writeUTFString",
                   "help": "Writes an UTF string into the socket.",
                   "params": [
                       "datastring"
                   ]
               },
               {
                   "name": "connect",
                   "help": "Creates a socket and connects it to a remote server, within the given timeout delay (a time out value of 0 means infinite timeout).",
                   "params": [
                       "localip",
                       "localport",
                       "remoteip",
                       "remoteport",
                       "timeout"
                   ]
               }
           ],
           "tests": [
               {
                   "name": "isOpen",
                   "help": "True is the socket is currently open, false otherwise; or the contrary of option not is activated",
                   "params": [
                       "options"
                   ]
               },
               {
                   "name": "EOF",
                   "help": "End of stream has been reached.",
                   "params": [
                       "options"
                   ]
               },
               {
                   "name": "timedOut",
                   "help": "The previous accpet or read operation failed on time out.",
                   "params": [
                       "options"
                   ]
               }
           ]
       },
       {
           "name": "SvnInjector",
           "help": "This injector enables submitting check-out and commit requests to an SVN server. Important notice: this plug-in supports only a single virtual user at a given point in time; multiple virtual users can't use this plug-in simultaneously.",
           "params": [],
           "controls": [
               {
                   "name": "addDir",
                   "help": "Adds a new directory (to be committed later on). The full path must be provided, relative to the repository path. This directory is then considered as current directory with regard to further file addition.",
                   "params": [
                       "path"
                   ]
               },
               {
                   "name": "addFile",
                   "help": "Adds a new file (to be committed later on) to the current directory, i.e. either the root directory of the repository path or the latest added or set directory (see controls addDir/setDir).",
                   "params": [
                       "name",
                       "size"
                   ]
               },
               {
                   "name": "setDir",
                   "help": "Changes current directory with regard to file addition, without creating the directory. The full path must be provided, relative to the repository path.",
                   "params": [
                       "path"
                   ]
               },
               {
                   "name": "connect",
                   "help": "Connects to an SVN repository. Must be called prior to any other operation. Don't forget to close this connection to free resources once operations are complete.",
                   "params": [
                       "url",
                       "login",
                       "password"
                   ]
               },
               {
                   "name": "close",
                   "help": "Closes current SVN connection.",
                   "params": []
               }
           ],
           "samples": [
               {
                   "name": "checkout",
                   "help": "Performs a full, recursive check-out of the target repository URL. All contents retrieved from the repository are discarded. In the generated sample/action report, the default comment is the repository URL.",
                   "params": [
                       "type",
                       "comment",
                       "path"
                   ]
               },
               {
                   "name": "commit",
                   "help": "Commits changes defined by previous controls (addFile, addDir)",
                   "params": [
                       "message",
                       "type",
                       "comment"
                   ]
               }
           ]
       },
       {
           "name": "TcpInjector",
           "help": "Give object help.",
           "params": [
               "charset",
               "unit"
           ],
           "controls": [
               {
                   "name": "accept",
                   "help": "Give control help.",
                   "params": [
                       "localip",
                       "localport",
                       "timeout"
                   ]
               },
               {
                   "name": "setTimeOut",
                   "help": "Give control help.",
                   "params": [
                       "timeout"
                   ]
               },
               {
                   "name": "connect",
                   "help": "Creates a socket and connects it to a remote server, whitin the given timeout delay ( a time out value of 0 means infinite timeout).",
                   "params": [
                       "localip",
                       "localport",
                       "remoteip",
                       "remoteport",
                       "timeout"
                   ]
               },
               {
                   "name": "close",
                   "help": "Give control help.",
                   "params": []
               }
           ],
           "samples": [
               {
                   "name": "readBytes",
                   "help": "Reads an byte string from the socket.",
                   "params": [
                       "buffername"
                   ]
               },
               {
                   "name": "readUTFString",
                   "help": "Reads an UTF string from the socket.",
                   "params": [
                       "buffername"
                   ]
               },
               {
                   "name": "readStringLine",
                   "help": "Reads an line string from the socket.",
                   "params": [
                       "buffername"
                   ]
               },
               {
                   "name": "writeBytes",
                   "help": "Writes an Byte string into the socket.",
                   "params": [
                       "datastring"
                   ]
               },
               {
                   "name": "writeUTFString",
                   "help": "Writes an UTF string into the socket.",
                   "params": [
                       "datastring"
                   ]
               },
               {
                   "name": "writeStringLine",
                   "help": "Writes an line string into the socket.",
                   "params": [
                       "datastring"
                   ]
               },
               {
                   "name": "sendRequestLine",
                   "help": "Writes an line string into the socket and Reads an line string from the socket.",
                   "params": [
                       "buffername",
                       "datastring"
                   ]
               }
           ],
           "tests": [
               {
                   "name": "isOpen",
                   "help": "Give test help.",
                   "params": [
                       "options"
                   ]
               },
               {
                   "name": "EOF",
                   "help": "Give test help.",
                   "params": [
                       "options"
                   ]
               },
               {
                   "name": "timedOut",
                   "help": "Give test help.",
                   "params": [
                       "options"
                   ]
               }
           ]
       },
       {
           "name": "UdpInjector",
           "help": "Traffic injector for UDP protocol. Consider increasing the maximum number of UDP sockets in all Java runtimes involved in your test plans, by setting property sun.net.maxDatagramSockets, since the default is quite low in most Java runtimes.",
           "params": [
               "charset",
               "unit"
           ],
           "controls": [
               {
                   "name": "bind",
                   "help": "binds this datagram socket to a local address",
                   "params": [
                       "host",
                       "port"
                   ]
               },
               {
                   "name": "close",
                   "help": "closes this datagram socket",
                   "params": []
               },
               {
                   "name": "disconnect",
                   "help": "disconnects this datagram socket from the remote address it is currently connected to (does nothing if it is not connected).",
                   "params": []
               },
               {
                   "name": "setRcvBufSize",
                   "help": "Advises the receive buffer size in bytes.",
                   "params": [
                       "size"
                   ]
               },
               {
                   "name": "setSndBufSize",
                   "help": "Advises the send buffer size in bytes.",
                   "params": [
                       "size"
                   ]
               },
               {
                   "name": "setTimeOut",
                   "help": "Sets the receive time out in millisesonds. A zero value means infinite time out.",
                   "params": [
                       "timeout"
                   ]
               },
               {
                   "name": "clear",
                   "help": "Clears a buffer previously stored by a call to receive.",
                   "params": [
                       "variable"
                   ]
               },
               {
                   "name": "clearAll",
                   "help": "Clears all buffers previously stored by calls to receive.",
                   "params": []
               }
           ],
           "samples": [
               {
                   "name": "connect",
                   "help": "connects this datagram socket to a remote address",
                   "params": [
                       "host",
                       "port"
                   ]
               },
               {
                   "name": "connect2sender",
                   "help": "connects this datagram socket to the address of the sender of the last packet received.",
                   "params": []
               },
               {
                   "name": "send",
                   "help": "sends a datagram packet through this socket",
                   "params": [
                       "data",
                       "comment"
                   ]
               },
               {
                   "name": "receive",
                   "help": "Waits receiving a datagram packet on this socket according to current timeout setting. Data read from this socket may be optionally stored in a named buffer for further reference through a variable expression ${piId:buffer_name}.",
                   "params": [
                       "variable",
                       "comment"
                   ]
               },
               {
                   "name": "steal",
                   "help": "Waits receiving a datagram packet from another socket identified via its port number. The timeout is the one defined by the stolen socket itself. Data read from this socket may be optionally stored in a named buffer for further reference through a variable expression ${piId:buffer_name}.",
                   "params": [
                       "port",
                       "buffer",
                       "comment"
                   ]
               }
           ],
           "tests": [
               {
                   "name": "isConnected",
                   "help": "True if this datagram socket is currently connected, false otherwise.",
                   "params": []
               },
               {
                   "name": "isNotConnected",
                   "help": "False if this datagram socket is currently connected, true otherwise.",
                   "params": []
               },
               {
                   "name": "isBound",
                   "help": "True if this datagram socket is currently bound to a local address, false otherwise.",
                   "params": []
               },
               {
                   "name": "isNotBound",
                   "help": "False if this datagram socket is currently bound to a local address, true otherwise.",
                   "params": []
               },
               {
                   "name": "isClosed",
                   "help": "True if this datagram socket is currently closed, false otherwise.",
                   "params": []
               },
               {
                   "name": "isNotClosed",
                   "help": "False if this datagram socket is currently closed, true otherwise.",
                   "params": []
               },
               {
                   "name": "isDefined",
                   "help": "True if the given buffer name has been defined by a previous call to receive, false otherwise.",
                   "params": [
                       "variable"
                   ]
               },
               {
                   "name": "isNotDefined",
                   "help": "True if the given buffer name has not been defined by a previous call to receive, false otherwise.",
                   "params": [
                       "variable"
                   ]
               }
           ]
       },
       {
           "name": "Chrono",
           "help": "This plug-in makes it possible to measure the duration of a behavior segment and get a pseudo-sample event with this duration value. Once the chronograph is started by the appropriate control, the current duration value can be recorded by the split sample, and can be used as variable ${my_chrono:}.",
           "params": [],
           "controls": [
               {
                   "name": "start",
                   "help": "starts the chrono",
                   "params": []
               },
               {
                   "name": "suspend",
                   "help": "suspends the chrono",
                   "params": []
               },
               {
                   "name": "resume",
                   "help": "resumes the chrono",
                   "params": []
               },
               {
                   "name": "drop",
                   "help": "stops the chrono without creating a pseudo-sample",
                   "params": []
               }
           ],
           "samples": [
               {
                   "name": "split",
                   "help": "records current chrono value in a pseudo sample without stopping the chrono.",
                   "params": [
                       "successful",
                       "comment",
                       "result"
                   ]
               },
               {
                   "name": "stop",
                   "help": "records current chrono value in a pseudo sample and stops the chrono.",
                   "params": [
                       "successful",
                       "comment",
                       "result"
                   ]
               }
           ],
           "tests": [
               {
                   "name": "is_gt",
                   "help": "current chrono value is greater than the given value",
                   "params": [
                       "value"
                   ]
               },
               {
                   "name": "is_gte",
                   "help": "current chrono value is greater than or equals the given value",
                   "params": [
                       "value"
                   ]
               },
               {
                   "name": "is_lt",
                   "help": "current chrono value is less than the given value",
                   "params": [
                       "value"
                   ]
               },
               {
                   "name": "is_lte",
                   "help": "current chrono value is greater than or equals the given value",
                   "params": [
                       "value"
                   ]
               },
               {
                   "name": "is_on",
                   "help": "chrono is started",
                   "params": []
               },
               {
                   "name": "is_off",
                   "help": "chrono is stopped",
                   "params": []
               },
               {
                   "name": "is_suspended",
                   "help": "chrono is suspended",
                   "params": []
               }
           ]
       },
       {
           "name": "Common",
           "help": "Provides constants TRUE and FALSE to be used as conditions, as well as miscellaneous features: printing, alarm, logging and Java system property setting. Also provides a \"date_ms\" variable that gives the current date as the number of milliseconds elapsed since 1st January 1970 GMT (syntax: ${common_plugin_id:date_ms}). A custom-formatted date can be obtained with variable \"date!myDateFormat\", where myDateFormat follows the Java java.text.SimpleDateFormat specification.",
           "params": [],
           "controls": [
               {
                   "name": "printout",
                   "help": "Prints a string on standard output",
                   "params": [
                       "string"
                   ]
               },
               {
                   "name": "printerr",
                   "help": "Prints a string on error output",
                   "params": [
                       "string"
                   ]
               },
               {
                   "name": "alarm",
                   "help": "Generates an alarm with the given message",
                   "params": [
                       "message"
                   ]
               },
               {
                   "name": "setProperty",
                   "help": "Sets a Java system property.",
                   "params": [
                       "name",
                       "value"
                   ]
               },
               {
                   "name": "setTimeZone",
                   "help": "Sets timezone for variable 'date'. The timezone parameter may be filled a number of ways: either an abbreviation such as \"PST\", a full name such as \"Europe/Paris\", or a custom ID such as \"GMT+1:00\". Leave this parameter empty to get the default timezone as configured on the host.",
                   "params": [
                       "timezone"
                   ]
               }
           ],
           "samples": [
               {
                   "name": "log",
                   "help": "Generates a pseudo action/request report for custom logging purpose.",
                   "params": [
                       "duration",
                       "comment",
                       "result",
                       "successful",
                       "iteration"
                   ]
               }
           ],
           "tests": [
               {
                   "name": true,
                   "help": "A condition which is always true.",
                   "params": []
               },
               {
                   "name": false,
                   "help": "A condition which is always false.",
                   "params": []
               }
           ]
       },
       {
           "name": "ConstantTimer",
           "help": "Provides a fixed duration timer, either to get a constant think time (see sleep timer), or to get a fixed (minimum) time for a behavior sub-section (see period_begin/period_end timers). Also supports a basic timeout feature through conditions to check if some execution time has elapsed for a delimited behavior sub-section. All durations are in milliseconds.",
           "params": [
               "duration_arg"
           ],
           "samples": [
               {
                   "name": "reset",
                   "help": "Pseudo-sample: initializes a time-out timer, using the latest value set, if any, or the value set at plug-in import otherwise.",
                   "params": []
               },
               {
                   "name": "set",
                   "help": "Pseudo sample: initializes a time-out timer, with the provided value.",
                   "params": [
                       "duration_arg"
                   ]
               }
           ],
           "tests": [
               {
                   "name": "running",
                   "help": "True if a time-out timer has been initialized and its value has not been reached yet, false otherwise.",
                   "params": []
               },
               {
                   "name": "passed",
                   "help": "True if a time-out timer has been initialized and its value has been reached, false otherwise.",
                   "params": []
               }
           ],
           "timers": [
               {
                   "name": "sleep",
                   "help": "Sleeps for the given time, if any, or the default time set at plug-in import otherwise.",
                   "params": [
                       "duration_arg"
                   ]
               },
               {
                   "name": "period_begin",
                   "help": "Pseudo timer that performs no think time but simply sets a period_begin mark.",
                   "params": []
               },
               {
                   "name": "period_end",
                   "help": "Sleeps during sufficient time so that the execution time elapsed from the latest period_begin mark and this period_end included is at least the specified duration, if any, or the default timer value set at plug-in import otherwise. Of course, it may happen that the execution time of the delimited block is greater than the required duration, in which case period_end does no sleep at all, but the delimited block is fully executed however (should you want a more preemptive construct, include this block in an ISAC's preemption statement, using this plug-in's time-out condition).",
                   "params": [
                       "period_arg"
                   ]
               }
           ]
       },
       {
           "name": "Context",
           "help": "Defines arbitrary variables with initial values. These variables may be loaded from a Java-style property file or set on plug-in import. Empty value is not supported: setting an empty value to a variable turns to clear the variable itself (it is no longer considered as defined). Similarly, getting a value from an undefined variable returns an empty string value. Any of these variables may be used in any plug-in parameter by referencing the variable through the following expression: ${this context plug-in identifier:variable's name}. Example: ${fooplugin:barvar}",
           "params": [
               "variables",
               "files",
               "shared"
           ],
           "controls": [
               {
                   "name": "clear",
                   "help": "Remove all variable definitions.",
                   "params": []
               },
               {
                   "name": "dup",
                   "help": "Duplicates a variable into a new variable with the same value.",
                   "params": [
                       "from",
                       "to"
                   ]
               },
               {
                   "name": "load",
                   "help": "Set variables by loading a property file.",
                   "params": [
                       "file"
                   ]
               },
               {
                   "name": "set",
                   "help": "Sets a variable's value. Entering an empty value results in clearing the variable.",
                   "params": [
                       "variable",
                       "value"
                   ]
               }
           ],
           "tests": [
               {
                   "name": "isdef",
                   "help": "Checks that the given variable is defined",
                   "params": [
                       "variable"
                   ]
               },
               {
                   "name": "isnotdef",
                   "help": "Checks that the given variable is not defined",
                   "params": [
                       "variable"
                   ]
               }
           ]
       },
       {
           "name": "Counter",
           "help": "Provides an integer counter, with associated primitives for changing and testing its value. Each virtual user may manage its own counter instance, or share a single counter, which can be useful to handle unique identifiers, for example. Since the counter value is held by a multi-precision integer, it has no limit value. When setting, retrieving or comparing the counter value, the value is formatted accordingly to the radix set on import. The counter value can be obtained through the variable expression ${counter_id:}, or with a format specification ${counter_id:n} where n is the size of the resulting string (white spaces are added at the head of the string to reach the specified number of characters). Examples: ${counter_id:8} will be substituted by the string \" 1234567\" if the counter value is 1234567, or \"123456789\" if the value is 123456789 (no truncation). It is also possible to perform atomic \"get-and-set\" operations, through variable expressions ${counter_id:++} and ${counter_id:--}. These variables are replaced by the current counter value, and the counter value is incremented (respectively decremented) in an atomic manner (i.e. when the counter is shared, no other virtual user can concurrently get or change the counter value). This is just a convenience for an unshared counter, while it is a necessary construct for managing unique identifiers through all vUsers sharing a counter. A Counter default value is 0 (zero), unless another default value is specified on import.",
           "params": [
               "value_arg",
               "shared_arg",
               "radix_arg"
           ],
           "controls": [
               {
                   "name": "reset",
                   "help": "Sets this Counter's value to the default value (as set on plug-in import).",
                   "params": []
               },
               {
                   "name": "set",
                   "help": "Sets this Counter's value to the provided Integer.",
                   "params": [
                       "value_arg"
                   ]
               },
               {
                   "name": "inc",
                   "help": "Increments this Counter's value.",
                   "params": []
               },
               {
                   "name": "dec",
                   "help": "Decrements this Counter's value.",
                   "params": []
               },
               {
                   "name": "add",
                   "help": "Adds the provided Integer to this Counter's value.",
                   "params": [
                       "value_arg"
                   ]
               },
               {
                   "name": "sub",
                   "help": "Subtracts the provided Integer to the current Counter's value.",
                   "params": [
                       "value_arg"
                   ]
               },
               {
                   "name": "multiply",
                   "help": "Multiplies this Counter's value by the provided Integer.",
                   "params": [
                       "integer"
                   ]
               },
               {
                   "name": "div",
                   "help": "Divides this Counter's value by the provided Integer. This is an \"integer division\", which means the result is the integer part of the division result.",
                   "params": [
                       "integer"
                   ]
               },
               {
                   "name": "mod",
                   "help": "Sets this Counter's value to the remainder of the integer division of current value by the provided integer.",
                   "params": [
                       "integer"
                   ]
               }
           ],
           "tests": [
               {
                   "name": "equals",
                   "help": "True when the counter equals the given value, false otherwise.",
                   "params": [
                       "value_arg"
                   ]
               },
               {
                   "name": "not_equal",
                   "help": "True when the counter is different from the given value, false otherwise.",
                   "params": [
                       "value_arg"
                   ]
               },
               {
                   "name": "gt",
                   "help": "True when the counter is strictly greater than the given value, false otherwise.",
                   "params": [
                       "value_arg"
                   ]
               },
               {
                   "name": "gte",
                   "help": "True when the counter is greater than, or equal to the given value, false otherwise.",
                   "params": [
                       "value_arg"
                   ]
               },
               {
                   "name": "lt",
                   "help": "True when the counter is strictly less than the given value, false otherwise.",
                   "params": [
                       "value_arg"
                   ]
               },
               {
                   "name": "lte",
                   "help": "True when the counter is less than, or equal to the given value, false otherwise.",
                   "params": [
                       "value_arg"
                   ]
               },
               {
                   "name": "is_zero",
                   "help": "True when the counter equals zero, false otherwise.",
                   "params": []
               },
               {
                   "name": "is_not_zero",
                   "help": "True when the counter is different from zero, false otherwise.",
                   "params": []
               },
               {
                   "name": "gt0",
                   "help": "True when the counter is strictly greater than zero, false otherwise.",
                   "params": []
               },
               {
                   "name": "gte0",
                   "help": "True when the counter is greater than, or equal to zero, false otherwise.",
                   "params": []
               },
               {
                   "name": "lt0",
                   "help": "True when the counter is strictly less than zero, false otherwise.",
                   "params": []
               },
               {
                   "name": "lte0",
                   "help": "True when the counter is less than, or equal to zero, false otherwise.",
                   "params": []
               },
               {
                   "name": "test_and_set",
                   "help": "True when the counter's value equals the given value, false otherwise. Atomic operation, setting the counter to a new value when it equals a given value. This operation is mostly useful when the counter is used in shared mode.",
                   "params": [
                       "test_value",
                       "new_value"
                   ]
               },
               {
                   "name": "not_test_and_set",
                   "help": "True when the counter's value differs from the given value, false otherwise. Atomic operation, setting the counter to a new value when it equals a given value. This operation is mostly useful when the counter is used in shared mode.",
                   "params": [
                       "test_value",
                       "new_value"
                   ]
               }
           ]
       },
       {
           "name": "CsvProvider",
           "help": "Provides a data set whose data are read from a CSV (comma-separated values) file. Call the \"next\" control to get the next line from the file. It must be called at least once prior to getting any data. Control \"skip\" gets the nth next line. The \"reset\" control goes back to the first line. For each line, fields/values are available through variables: - either with array-like notation [0], [1] etc. (e.g. ${plugin_id:[0]}) - or with chosen names when defined in plug-in import parameters (e.g. {plugin_id:phone_field}) The number of lines is available through a variable named # (e.g. ${pluginId:#}), unless the \"load at runtime\" option is enabled, in which case this variable is not available. Parameters are: - filename (required): the full path to the CSV file. - separator (optional): the separator character. The default separator is the comma (','). - fields (optional): names for the variables, separated by the defined separator. - macintosh_line_separator: use CR instead of LF as line separator. - shared: when set, progression in the lines is shared by all session objects. In other words, each session object will get a different line instead of all getting the same sequence of lines. - loop: if set, the line sequence wraps up to the first line when the end of file is reached. Otherwise, an alarm is thrown when trying to get a field value while the end of file has been reached, and the empty string is used as value. - load at runtime: when enabled, lines are read from the file one-by-one, when the \"next\" or \"skip\" control is called. Otherwise, all lines are read at once at deployment time. This option is recommended for very big files, in order to save deployment time and memory space. But then, it is recommended to enable the \"shared\" mode in order to avoid a proliferation of file descriptors, especially when you run a great number of virtual users.",
           "params": [
               "filename",
               "separator",
               "macintosh_line_separator",
               "shared",
               "loop",
               "fields",
               "comment",
               "bigfile"
           ],
           "controls": [
               {
                   "name": "next",
                   "help": "Go to next line. When loop is enabled, wraps up to the first line once the last line has been read.",
                   "params": []
               },
               {
                   "name": "reset",
                   "help": "Jump to the first line.",
                   "params": []
               },
               {
                   "name": "skip",
                   "help": "Skips the given number of lines (equivalent to n 'next' calls), looping to the first line if the loop option has been set for this CsvProvider.",
                   "params": [
                       "n"
                   ]
               }
           ],
           "tests": [
               {
                   "name": "endOfFile",
                   "help": "True if there is no more line available (including at current position), false if current line has a value. Always false when loop option is enabled.",
                   "params": []
               },
               {
                   "name": "notEndOfFile",
                   "help": "True if current line has a value, false if the end of the CSV file has been reached. Always true when loop option is enabled.",
                   "params": []
               },
               {
                   "name": "isDefined",
                   "help": "True if the given field or column number was present in the latest line read, false if it was absent",
                   "params": [
                       "field"
                   ]
               },
               {
                   "name": "isNotDefined",
                   "help": "True if the given field or column number was absent from the latest line read, false if it was present",
                   "params": [
                       "field"
                   ]
               }
           ]
       },
       {
           "name": "FileHandler",
           "help": "Provides conditions and controls for handling files and directories. Some file attibutes are available through variable expressions ${pluginId:/some/path;attribute}, where attribute is one of the following: - size (file size in number of bytes) - ctime (file creation time in ms) - mtime (file last modification time in ms) Example: ${myFileHandler:/some/path;size}",
           "params": [],
           "controls": [
               {
                   "name": "deleteFiles",
                   "help": "Delete files.",
                   "params": [
                       "paths"
                   ]
               },
               {
                   "name": "deleteEmptyDir",
                   "help": "Delete an empty directory.",
                   "params": [
                       "path"
                   ]
               }
           ],
           "tests": [
               {
                   "name": "exists",
                   "help": "True if the given path exists, false otherwise.",
                   "params": [
                       "path"
                   ]
               },
               {
                   "name": "existsNot",
                   "help": "True if the given path does not exist, false otherwise.",
                   "params": [
                       "path"
                   ]
               },
               {
                   "name": "readable",
                   "help": "True if the file at the given path is readable, false otherwise.",
                   "params": [
                       "path"
                   ]
               },
               {
                   "name": "notReadable",
                   "help": "True if the file at the given path is not readable, false otherwise.",
                   "params": [
                       "path"
                   ]
               },
               {
                   "name": "writeable",
                   "help": "True if the file at the given path is writeable, false otherwise.",
                   "params": [
                       "path"
                   ]
               },
               {
                   "name": "notWriteable",
                   "help": "True if the file at the given path is not writeable, false otherwise.",
                   "params": [
                       "path"
                   ]
               },
               {
                   "name": "isDir",
                   "help": "True if the given path is a directory, false otherwise.",
                   "params": [
                       "path"
                   ]
               },
               {
                   "name": "isFile",
                   "help": "True if the given path is a file, false otherwise.",
                   "params": [
                       "path"
                   ]
               },
               {
                   "name": "isLink",
                   "help": "True if the given path is a symbolic link, false otherwise.",
                   "params": [
                       "path"
                   ]
               },
               {
                   "name": "isNotLink",
                   "help": "True if the given path is not a symbolic link, false otherwise.",
                   "params": [
                       "path"
                   ]
               }
           ]
       },
       {
           "name": "FileReader",
           "help": "Reads the full content of a file. Then, this content can be accessed through a variable expression ${myFileReaderId:}.",
           "params": [
               "filename",
               "charset"
           ],
           "controls": [
               {
                   "name": "clear",
                   "help": "Discards any loaded content, and considers this file reader as unset.",
                   "params": []
               },
               {
                   "name": "load",
                   "help": "Reads the full content of a file. Then, this content can be accessed through a variable expression ${myFileReaderId:}. If the file content can't be loaded for any reason, any previously loaded content is discarded (in other words, this file reader is cleared).",
                   "params": [
                       "filename",
                       "charset"
                   ]
               }
           ],
           "tests": [
               {
                   "name": "isSet",
                   "help": "True if a file has been successfully loaded, false otherwise.",
                   "params": []
               },
               {
                   "name": "isNotSet",
                   "help": "True if no file has been successfully loaded, false otherwise.",
                   "params": []
               }
           ]
       },
       {
           "name": "FileWriter",
           "help": "This plug-in supports writing data to a local file, either in overwriting or appending mode. This plug-in provides four variables: pathname (the full path to the file, including the file name), dirname (the path to the directory holding this file), basename (just the name of the file without its directory's path) and length (the file size in bytes).",
           "params": [
               "charset"
           ],
           "controls": [
               {
                   "name": "open",
                   "help": "Opens a file for writing data, with the provided path name. Both absolute and relative path names are supported. The file is created if it does not exist already. If it already exists, its current content is either overwritten (overwrite option) or kept (append option).",
                   "params": [
                       "append",
                       "path"
                   ]
               },
               {
                   "name": "write",
                   "help": "Writes the provided string to the currently open file.",
                   "params": [
                       "string"
                   ]
               },
               {
                   "name": "writeln",
                   "help": "Writes the provided string to the currently open file, and terminates current line.",
                   "params": [
                       "string"
                   ]
               },
               {
                   "name": "newline",
                   "help": "Terminates current line.",
                   "params": []
               },
               {
                   "name": "flush",
                   "help": "Flushes possibly unwritten data to the file system.",
                   "params": []
               },
               {
                   "name": "close",
                   "help": "Closes current file, flushing possibly unwritten data. Any further writing to the file requires to re-open it.",
                   "params": []
               }
           ],
           "tests": [
               {
                   "name": "isOpen",
                   "help": "True when a file has been successfully open or created for writing, false otherwise.",
                   "params": []
               },
               {
                   "name": "isNotOpen",
                   "help": "True if no file is currently open for writing, false otherwise.",
                   "params": []
               },
               {
                   "name": "exists",
                   "help": "True if the designated file exists, false otherwise.",
                   "params": [
                       "path"
                   ]
               },
               {
                   "name": "existsNot",
                   "help": "True if the designated file does not exists, false otherwise.",
                   "params": [
                       "path"
                   ]
               }
           ]
       },
       {
           "name": "IpProvider",
           "help": "Provides locally available IP addresses, either in a round-robin or in a random way, accordlingly to the chosen policy. Delivered addresses may or may not be unique among virtual users, accordingly to the \"unique\" switch. IP addresses may be restricted to one or several subnetworks through the optional setting of filters. The obtained IP address is available through an empty variable notation ${ipprovider-id:}.",
           "params": [
               "policy",
               "filters",
               "unique"
           ],
           "controls": [
               {
                   "name": "release",
                   "help": "Releases the address that had been previously obtained by this virtual user. Ignored of the virtual user has not got an IP address.",
                   "params": []
               },
               {
                   "name": "next",
                   "help": "Gets a locally available IP address, according to the attribution policy and possible subnetwork filters. This address may also be given or not to another virtual user according to the \"unique\" switch setting. Getting a new IP address automatically releases current IP address (if any) previously obtained by this virtual user.",
                   "params": []
               }
           ]
       },
       {
           "name": "JsonHandler",
           "help": "This plug-in supports easy data extraction from a JSON structure, using the JsonPath syntax. Refer to http://goessner.net/articles/JsonPath/ for JsonPath description.",
           "params": [],
           "controls": [
               {
                   "name": "parse",
                   "help": "Parses current JSON content using the given JsonPath. Result(s) are stored in a variable - a result set - whose name must be provided. If there is no result, the variable won't be defined. Otherwise, the variable resolves to the first result set's value. Then, control nextResult allows for iterating over next values. Finally, the result set variable is no longer defined when no more value is available. In addition, the number of currently available values in a result set can be retrieved from another variable whose name is the result set name prefixed by # (for example, #myparseresult).",
                   "params": [
                       "jsonpath",
                       "resultset"
                   ]
               },
               {
                   "name": "setJsonContent",
                   "help": "Sets the JSON structure to parse.",
                   "params": [
                       "jsoncontent"
                   ]
               },
               {
                   "name": "clearAll",
                   "help": "Clears this JsonPath parser state, i.e. its Json content is set to the empty string, and all result sets from previous parsing are discarded.",
                   "params": []
               },
               {
                   "name": "clearResultSet",
                   "help": "Clears the given result set.",
                   "params": [
                       "resultset"
                   ]
               },
               {
                   "name": "nextResult",
                   "help": "Switches to the next value in the provided result set.",
                   "params": [
                       "resultset"
                   ]
               }
           ],
           "tests": [
               {
                   "name": "isDefined",
                   "help": "True if the provided result set is defined and holds a current value, false otherwise. When true, a variable reference to this result set returns current value of the result set.",
                   "params": [
                       "resultset"
                   ]
               },
               {
                   "name": "isNotDefined",
                   "help": "True if the provided result set is undefined or has no more value available, false otherwise. When true, any variable reference to the result set will throw an exception.",
                   "params": [
                       "resultset"
                   ]
               }
           ]
       },
       {
           "name": "Random",
           "help": "Provides random timers, data and dummy samples whose duration follows a given probability distribution. Five distribution laws are available: - Dirac distribution (not actually random: constant value), - uniform distribution, - Poisson distribution, - gaussian distribution, - negative exponential distribution. Use control primitives to set the distribution law and parameters to be used. Then, random sleeps (think times) and dummy actions with random response time can be invoked. Moreover, random integer values are available by getting the 'int' variable (through expression ${...:int}), and random strings of random size (according to the distribution law settings) are available through the 'string' variable (through expression ${...:string}). Finally, you can also retrieve a random v4 UUID String through the 'uuid' variable (this UUID is independent from the distribution law).",
           "params": [],
           "controls": [
               {
                   "name": "setDirac",
                   "help": "Sets the Dirac random distribution law (actually not really random) Parameters: - value (required): the constant value the generator will always give.",
                   "params": [
                       "value"
                   ]
               },
               {
                   "name": "setUniform",
                   "help": "Sets the uniform random distribution law. Parameters: - min (required): the minimal integer value. - max (required): the maximal integer value.",
                   "params": [
                       "min",
                       "max"
                   ]
               },
               {
                   "name": "setPoisson",
                   "help": "Sets the Poisson random distribution law. Parameters : - unit (optional): the generated values are multipied by this factor. The default value is 1. - parameter (required): the parameter (mean and variance) of the Poisson distribution in the specified unit.",
                   "params": [
                       "unit",
                       "parameter"
                   ]
               },
               {
                   "name": "setGaussian",
                   "help": "Sets the Gaussian random distribution law. Parameters: - min: minimum value - max: maximum value - mean: mean value - deviation: values deviation",
                   "params": [
                       "min",
                       "max",
                       "mean",
                       "deviation"
                   ]
               },
               {
                   "name": "setNegativeExpo",
                   "help": "Sets the negative exponential random distribution law. Parameters: - min: minimum value - mean: mean value",
                   "params": [
                       "min",
                       "mean"
                   ]
               }
           ],
           "samples": [
               {
                   "name": "action",
                   "help": "Dummy action whose response time is given by a random value in ms, according to current distribution law settings. Parameters: - successful: set whether this action must be considered as successfully completed or not - comment: give an arbitray comment String to this dummy action - type (optional): custom action type. The default value is \"dummy action\"",
                   "params": [
                       "successful",
                       "comment",
                       "type"
                   ]
               }
           ],
           "timers": [
               {
                   "name": "sleep",
                   "help": "Timer whose duration is given by a random value in ms, according to current distribution law settings.",
                   "params": []
               }
           ]
       },
       {
           "name": "StringHandler",
           "help": "Provides a collection of primitives for generating, modifying and extracting Strings. This plug-in provides the following data: - ${handler_id:} gives the current string's value - ${handler_id:#} gives the current string's length and, after a call to control \"slice\": - ${handler_id:slice#N} gives the Nth string slice - ${handler_id:slice++} gives the next string slice - ${handler_id:slice#} gives the number of string slices - ${handler_id:slice} gives the current string slice after a call to \"nextSlice\"",
           "params": [
               "default"
           ],
           "controls": [
               {
                   "name": "set",
                   "help": "Sets the string value.",
                   "params": [
                       "value"
                   ]
               },
               {
                   "name": "base64decode",
                   "help": "Decodes and replaces this string's value as if it was a base64-encoded String.",
                   "params": [
                       "charset",
                       "urlsafe"
                   ]
               },
               {
                   "name": "base64encode",
                   "help": "Encodes and replaces this string's value according to base64-encoding.",
                   "params": [
                       "charset",
                       "options"
                   ]
               },
               {
                   "name": "append",
                   "help": "Appends the provided string to current value.",
                   "params": [
                       "string"
                   ]
               },
               {
                   "name": "insert",
                   "help": "Inserts the provided string at the beginning.",
                   "params": [
                       "string"
                   ]
               },
               {
                   "name": "write",
                   "help": "Overwrites the string value from its beginning with the given string. If the provided string is longer than current value, then the resulting value equals the provided string. If the provided string is shorter than current value, then the resulting value ends with the trailing characters from current value.",
                   "params": [
                       "string"
                   ]
               },
               {
                   "name": "cut",
                   "help": "Cuts heading and trailing characters of current string.",
                   "params": [
                       "head",
                       "tail"
                   ]
               },
               {
                   "name": "truncate",
                   "help": "Truncate the string to a given length.",
                   "params": [
                       "length"
                   ]
               },
               {
                   "name": "replaceAll",
                   "help": "Replaces all occurrences of a string by another string.",
                   "params": [
                       "search",
                       "replace"
                   ]
               },
               {
                   "name": "random-set",
                   "help": "Sets the string value with a randmly generated string of a given size.",
                   "params": [
                       "size"
                   ]
               },
               {
                   "name": "setPattern",
                   "help": "Creates a new pattern (aka regular expression) for further search and capture of elements in the current string. Use \"match\" to trigger the next matching attempt, or \"slice\" to split the string into substrings, considering the pattern as a separator. Patterns are based on Java's java.util.regex package. http://docs.oracle.com/javase/1.6.0/docs/api/java/util/regex/Pattern.html",
                   "params": [
                       "pattern"
                   ]
               },
               {
                   "name": "resetMatching",
                   "help": "Resets the pattern matching process at the beginning of the string, using the previously set pattern.",
                   "params": []
               },
               {
                   "name": "match",
                   "help": "Finds the next match of current pattern, optionally skipping a number of matches. When necessary and only if matching is successful, use \"capture\" to extract elements from the matching substring.",
                   "params": [
                       "skip"
                   ]
               },
               {
                   "name": "capture",
                   "help": "Sets a variable with the substring that matched current pattern, as stated in the regular expression by a capture group. This substring will then be available through expression ${handler_id:my_variable}.",
                   "params": [
                       "level",
                       "variable"
                   ]
               },
               {
                   "name": "replaceAllMatches",
                   "help": "Replaces all substrings that matches the current pattern with the given string.",
                   "params": [
                       "replace"
                   ]
               },
               {
                   "name": "slice",
                   "help": "Slices the current value into substrings, using the current pattern as a separator (see \"setPattern\"). Resulting slices are available through variables slice#1 ... slice#n, or slice for current slice when using \"nextSlice\". Variable slice++ gives next slice. Variable clice# gives the number of slices.",
                   "params": [
                       "options"
                   ]
               },
               {
                   "name": "nextSlice",
                   "help": "Give control help.",
                   "params": []
               },
               {
                   "name": "digest",
                   "help": "Replaces this string's current value with its digest (aka hash) according to the chosen algorithm.",
                   "params": [
                       "algo"
                   ]
               },
               {
                   "name": "URLdecode",
                   "help": "Decodes current string considered as URL-encoded.",
                   "params": [
                       "charset"
                   ]
               },
               {
                   "name": "URLencode",
                   "help": "Encodes current string accorgin to the URL encoding rules.",
                   "params": [
                       "charset"
                   ]
               }
           ],
           "tests": [
               {
                   "name": "contains",
                   "help": "True if current string contains the given string, false otherwise.",
                   "params": [
                       "search"
                   ]
               },
               {
                   "name": "containsNot",
                   "help": "False if current string contains the given string, true otherwise.",
                   "params": [
                       "search"
                   ]
               },
               {
                   "name": "equals",
                   "help": "True if current string equals the given string, false otherwise.",
                   "params": [
                       "string"
                   ]
               },
               {
                   "name": "equalsNot",
                   "help": "True if current string differs from the given string, false otherwise.",
                   "params": [
                       "string"
                   ]
               },
               {
                   "name": "equalsIgnoreCase",
                   "help": "True if current string equals the given string, whatever letters are upper case or lower case, false otherwise.",
                   "params": [
                       "string"
                   ]
               },
               {
                   "name": "equalsNotIgnoreCase",
                   "help": "True if current string differs from the given string, whatever letters are upper case or lower case, false otherwise.",
                   "params": [
                       "string"
                   ]
               },
               {
                   "name": "hasMoreSlice",
                   "help": "True if at least one more string slice is available through a subsequent call to nextSlice or reference to variable slice++.",
                   "params": []
               },
               {
                   "name": "hasNoMoreSlice",
                   "help": "True if no more string slice is available through a subsequent call to nextSlice or reference to variable slice++.",
                   "params": []
               },
               {
                   "name": "isEmpty",
                   "help": "True if current string is empty, false otherwise.",
                   "params": []
               },
               {
                   "name": "isNotEmpty",
                   "help": "True if current string is not empty, false otherwise.",
                   "params": []
               },
               {
                   "name": "lengthIs",
                   "help": "True if current string length equals the given value, false otherwise.",
                   "params": [
                       "length"
                   ]
               },
               {
                   "name": "lengthIsNot",
                   "help": "True if current string length differs from the given value, false otherwise.",
                   "params": [
                       "length"
                   ]
               },
               {
                   "name": "lengthIsGt",
                   "help": "True if current string length is strictly greater than the given value, false otherwise.",
                   "params": [
                       "length"
                   ]
               },
               {
                   "name": "lengthIsGte",
                   "help": "True if current string length is greater than, or equals, the given value, false otherwise.",
                   "params": [
                       "length"
                   ]
               },
               {
                   "name": "lengthIsLt",
                   "help": "True if current string length is strictly less than the given value, false otherwise.",
                   "params": [
                       "length"
                   ]
               },
               {
                   "name": "lengthIsLte",
                   "help": "True if current string length is less than, or equals, the given value, false otherwise.",
                   "params": [
                       "length"
                   ]
               },
               {
                   "name": "matched",
                   "help": "True if the last pattern matching was successful, false otherwise.",
                   "params": []
               },
               {
                   "name": "matchedNot",
                   "help": "False if the last pattern matching was successful, true otherwise.",
                   "params": []
               },
               {
                   "name": "isGt",
                   "help": "True if current string is greater than the given string, with regard to the alphabetical ordering, false otherwise.",
                   "params": [
                       "string"
                   ]
               },
               {
                   "name": "isLt",
                   "help": "True if current string is less than the given string, with regard to the alphabetical ordering, false otherwise.",
                   "params": [
                       "string"
                   ]
               },
               {
                   "name": "isGte",
                   "help": "True if current string is greater than, or equals the given string, with regard to the alphabetical ordering, false otherwise.",
                   "params": [
                       "string"
                   ]
               },
               {
                   "name": "isLte",
                   "help": "True if current string is less than, or equals the given string, with regard to the alphabetical ordering, false otherwise.",
                   "params": [
                       "string"
                   ]
               }
           ]
       },
       {
           "name": "Synchro",
           "help": "Synchronization plug-in, for all virtual users of all behaviors of all deployed scenarios, either locally in current scenario, or, optionally, among several scenarios possibly distributed among several CLIF servers. The latter mode is enabled by selecting the \"distributed option\" on plug-in import and giving an arbitrary domain name. To support this distributed synchronization domain, the test plan must include a pseudo injector of class Synchro, with the chosen domain name as argument. You may deploy this Synchro pseudo-injector on any CLIF server in your test plan. Synchronization principles: named locks with wait/notify primitives. The status of a lock may also be tested. Synchronization can also take into account the number of notify calls on one lock. Rendez-vous provide an alternative usage, where the number of notify calls necessary to release the lock is predefined: either with the set-rendez-vous control primitive of this plug-in, or by adding lock_name=count arguments to the Synchro pseudo-injector argument line in the test plan. Some variables are provided: ${synchroId:} gives the list of released locks, ${synchroId:lock_name} gives the number of notifications for the specified lock. CAUTION: each virtual user blocked on a wait, waitN or rendez-vous call, keeps its own execution thread. As a consequence, the pool of ISAC execution threads must be sized in a relevant manner, with at least one execution thread per synchronizing virtual user. An alternative to avoid this constraint on threads is to make an active wait loop with a small sleep period, using wasNotified or wasNotifiedN condition.",
           "params": [
               "distributed",
               "domain"
           ],
           "controls": [
               {
                   "name": "notify",
                   "help": "Notifies the given lock, releasing all virtual users blocked waiting for this lock (possibly depending of the number of notify calls issued). This notification is remanent: further \"wait\" calls on this lock will not block the virtual user.",
                   "params": [
                       "lock"
                   ]
               },
               {
                   "name": "wait",
                   "help": "Waits until the given lock is notified, or the given time-out is elapsed (0 means infinite timeout, negative values are forbidden). If this lock has been notified before the \"wait\" call, the virtual user is not blocked (notifications are remanent).",
                   "params": [
                       "lock",
                       "timeout"
                   ]
               },
               {
                   "name": "waitN",
                   "help": "Waits until the given lock has been notified at least N times, or the given time-out is elapsed (0 means infinite timeout, negative values are forbidden).",
                   "params": [
                       "lock",
                       "timeout",
                       "times"
                   ]
               },
               {
                   "name": "set-rendez-vous",
                   "help": "Sets the number of times the given lock will have to be notified so that it is considered as released.",
                   "params": [
                       "lock",
                       "times"
                   ]
               },
               {
                   "name": "rendez-vous",
                   "help": "Notifies the given lock and then waits until it has been notified at least the number of times specified when the rendez-vous was set, or the given time-out is elapsed (0 means infinite timeout, negative values are forbidden). If the rendez-vous is not set yet, then this call first waits for the rendez-vous to be set (still meeting the time out).",
                   "params": [
                       "lock",
                       "timeout"
                   ]
               }
           ],
           "tests": [
               {
                   "name": "wasNotified",
                   "help": "True if the provided lock name has been released already, false otherwise. The \"not\" option just reverses the condition logic.",
                   "params": [
                       "options",
                       "lock"
                   ]
               },
               {
                   "name": "wasNotifiedN",
                   "help": "True if the provided lock name has been released already N times, false otherwise. The \"not\" option just reverses the condition logic.",
                   "params": [
                       "options",
                       "lock",
                       "times"
                   ]
               }
           ]
       }
   ]
}
END_OF_FILE
